<?php

namespace Last1971\AuthServer\Interfaces;

interface IBattleable
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string[]
     */
    public function getUserIds(): array;

    /**
     * @param string $id
     * @return void
     */
    public function setId(string $id): void;
}