<?php

namespace Last1971\AuthServer\Interfaces;

interface IUserable
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getPassword(): string;
}