<?php

use Last1971\SpaceBattle\Base\UObject;

return [
    '1' => new UObject([
        'id' => '1',
        'name' => 'First',
        'password' => md5('password'),
    ]),
    '2' => new UObject([
        'id' => '2',
        'name' => 'Second',
        'password' => md5('password'),
    ]),
];