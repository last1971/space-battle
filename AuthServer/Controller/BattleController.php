<?php

namespace Last1971\AuthServer\Controller;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Last1971\AuthServer\Commands\CreateBattle;
use Last1971\AuthServer\Interfaces\IBattleable;
use Last1971\SpaceBattle\Base\CommandAdapterFabric;
use Last1971\SpaceBattle\Base\DynamicAdapterClass;
use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Base\IoCRegister;
use Last1971\SpaceBattle\Base\Scope;
use Last1971\SpaceBattle\Base\UObject;
use Last1971\SpaceBattle\Interfaces\ICommand;

class BattleController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     * @throws \ReflectionException
     */
    public function create(Request $request)
    {
        $userIds = $request->get('userIds');
        $id = uniqid();
        $battle = new UObject(compact('userIds', 'id'));
        return $battle->get('id');
    }
}