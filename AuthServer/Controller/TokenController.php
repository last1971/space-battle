<?php

namespace Last1971\AuthServer\Controller;

use DateTimeImmutable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Last1971\SpaceBattle\Interfaces\IUObject;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

class TokenController extends Controller
{
    public function __construct(private array $battles)
    {
    }

    public function create(Request $request)
    {
        $gameId = $request->get('gameId');
        /** @var ?IUObject $battle */
        $battle = empty($this->battles[$gameId]) ? null : $this->battles[$gameId];
        if (!$battle) {
            return 'Bad gameId';
        }
        /** @var IUObject $user */
        $user = $request->user();
        $userId = $user->get('id');
        if (!in_array($userId, $battle->get('userIds'))) {
            return 'Bad user';
        }
        $configuration = Configuration::forAsymmetricSigner(
            new Sha256(),
            InMemory::plainText('my-secret'),
            InMemory::plainText('space-battle'),
        );
        $now = new DateTimeImmutable();
        $token = $configuration
            ->builder()
            ->expiresAt($now->modify('+1 hour'))
            ->withClaim('gameId', $gameId)
            ->getToken($configuration->signer(), $configuration->signingKey());
        return $token->toString();
    }
}