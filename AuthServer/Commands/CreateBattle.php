<?php

namespace Last1971\AuthServer\Commands;

use Exception;
use Last1971\AuthServer\Interfaces\IBattleable;
use Last1971\SpaceBattle\Interfaces\ICommand;

class CreateBattle implements ICommand
{
    public function __construct(private IBattleable $battleable)
    {
    }

    /**
     * @return void
     * @throws Exception
     */
    public function execute(): void
    {
        $s = $this->battleable->getUserIds();
        if (empty($this->battleable->getUserIds())) throw new Exception('Members empty');
        $this->battleable->setId(uniqid());
    }
}