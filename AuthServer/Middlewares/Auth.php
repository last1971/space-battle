<?php

namespace Last1971\AuthServer\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Last1971\AuthServer\Interfaces\IUserable;
use Last1971\SpaceBattle\Base\CommandAdapterFabric;
use Last1971\SpaceBattle\Interfaces\IUObject;

class Auth
{
    public function handle(Request $request, Closure $next, $guard = null)
    {
        /** @var IUObject[] $users */
        $users = require __DIR__ . '/../Config/users.php';
        $password = md5($request->get('password'));
        $name = $request->get('name');
        foreach ($users as $user) {
            if ($user->get('name') === $name && $user->get('password') === $password) {
                $request->setUserResolver(fn() => $user);
                return $next($request);
            }
        }
        return 'Bad credentials';
    }
}