<?php

namespace Last1971\SpaceBattle\Controlllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Commands\InterpretCommand;
use Last1971\SpaceBattle\DTOs\CommandMessageDTO;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class CommandMessageController extends Controller
{
    /**
     * @var IoC
     */
    private IoC $ioc;

    /**
     * @param IoC $ioc
     */
    public function __construct(IoC $ioc)
    {
        $this->ioc = $ioc;
    }

    /**
     * @throws UnknownProperties
     */
    public function commandMessage(Request $request)
    {
        $dto = CommandMessageDTO::fromRequest($request);
        $scopeCommand = $this->ioc->resolve('ScopesCurrent', $dto->gameId);
        if ($scopeCommand instanceof ICommand) {
            $scopeCommand->execute();
        }
        if ($this->ioc->resolve('GetObjects', $dto->objectId) && $this->ioc->resolve('GetCommandsQueue')) {
            $interpretCommand = new InterpretCommand($this->ioc, $dto);
            $commandsQueue = $this->ioc->resolve('GetCommandsQueue');
            $commandsQueue->push($interpretCommand);
        }
    }
}