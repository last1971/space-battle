<?php
/** @phan-suppress-next-line */
use Illuminate\Routing\Router;

/** @var $router Router */
$router->name('home')->get('/', function () {
    return 'hello world!';
});