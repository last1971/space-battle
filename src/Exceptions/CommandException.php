<?php

namespace Last1971\SpaceBattle\Exceptions;

use Exception;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Throwable;

class CommandException extends Exception
{
    private ICommand $command;

    public function __construct(ICommand $command, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->command = $command;
    }

    public function getCommand(): ICommand
    {
        return $this->command;
    }
}