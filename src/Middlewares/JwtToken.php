<?php

namespace Last1971\SpaceBattle\Middlewares;

use Closure;
use DateTime;
use Illuminate\Http\Request;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

class JwtToken
{
    /**
     * @param Request $request
     * @param Closure $next
     * @param $guard
     * @return mixed
     */
    /** @suppress PhanUndeclaredMethod */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        $token = $request->bearerToken();
        if (!$token) {
            return 'Error Authenticate.';
        }
        $configuration = Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText('space-battle')
        );
        $parsed = $configuration->parser()->parse($token);
        if ($parsed->isExpired(new DateTime())){
            return 'Token is rancid';
        }
        $tokenGameId = $parsed->claims()->get('gameId');
        $requestGameId = $request->get('gameId');
         if (!$tokenGameId || !$requestGameId || $tokenGameId !== $requestGameId) {
             return  'Bad gameId';
         }
        return $next($request);
    }
}