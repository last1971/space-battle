<?php

namespace Last1971\SpaceBattle\Expressions;

use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\Move;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExpression;
use Last1971\SpaceBattle\Interfaces\IMovable;
use Last1971\SpaceBattle\Interfaces\IUObject;

class StartMove implements IExpression
{
    /**
     * @param IUObject $context
     * @param IoC $ioc
     * @return ICommand
     */
    public function interpret(IUObject $context, IoC $ioc): ICommand
    {
        /** @var IUObject $object */
        $object = $ioc->resolve('GetObject', $context->get('id'));
        $object->set(
            'Velocity',
            new Vector([intval($context->get('initialVelocity')), intval($context->get('initialVelocity'))])
        );
        $movable = $ioc->resolve('Adapter', IMovable::class, $object);
        return new Move($movable);
    }
}