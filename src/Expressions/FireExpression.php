<?php

namespace Last1971\SpaceBattle\Expressions;

use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Commands\Fire;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExpression;
use Last1971\SpaceBattle\Interfaces\IUObject;

class FireExpression implements IExpression
{

    public function interpret(IUObject $context, IoC $ioc): ICommand
    {
        $object = $ioc->resolve('GetObject', $context->get('id'));
        return new Fire($object);
    }
}