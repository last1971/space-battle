<?php

namespace Last1971\SpaceBattle\Expressions;

use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Commands\Stop;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExpression;
use Last1971\SpaceBattle\Interfaces\IUObject;

class StopMove implements IExpression
{

    public function interpret(IUObject $context, IoC $ioc): ICommand
    {
        $object = $ioc->resolve('GetObject', $context->get('id'));
        return new Stop($object);
    }
}