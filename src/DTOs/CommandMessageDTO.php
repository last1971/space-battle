<?php

namespace Last1971\SpaceBattle\DTOs;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;
use Illuminate\Support\Collection;

class CommandMessageDTO extends DataTransferObject
{
    /**
     * @var string
     */
    public string $gameId;

    /**
     * @var string
     */
    public string $objectId;

    /**
     * @var string
     */
    public string $operationId;

    /**
     * @var Collection|null
     */
    public ?Collection $args;

    /**
     * @param Request $request
     * @return static
     * @throws UnknownProperties
     */
    public static function fromRequest(Request $request): self
    {
        return new self([
            'gameId' => $request->get('gameId'),
            'objectId' => $request->get('objectId'),
            'operationId' => $request->get('operationId'),
            'args' => $request->get('args') ? collect($request->get('args')) : null,
        ]);
    }
}