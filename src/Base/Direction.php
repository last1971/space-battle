<?php

namespace Last1971\SpaceBattle\Base;

class Direction
{
    /**
     * @var int
     */
    private int $direction;

    /**
     * @var int
     */
    private int $directionsNumber;

    /**
     * @param int $direction
     * @param int $directionsNumber
     * @throws \Exception
     */
    public function __construct(int $direction, int $directionsNumber)
    {
        $this->direction = $direction;
        $this->directionsNumber = $directionsNumber;
    }

    /**
     * @return int
     */
    public function getDirection(): int
    {
        return $this->direction;
    }

    /**
     * @return int
     */
    public function getDirectionsNumber(): int
    {
        return $this->directionsNumber;
    }

    /**
     * @param int $angularVelocity
     * @return void
     */
    public function next(int $angularVelocity): void
    {
        $this->direction = ($this->direction + $angularVelocity) % $this->directionsNumber;
    }
}