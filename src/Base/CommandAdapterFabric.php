<?php

namespace Last1971\SpaceBattle\Base;

use Last1971\SpaceBattle\Interfaces\IClassable;
use Last1971\SpaceBattle\Interfaces\IUObject;
use ReflectionClass;
use ReflectionException;

class CommandAdapterFabric
{
    private string $interface;

    private IUObject $object;

    private IoC $ioc;

    private IClassable $classable;

    /**
     * @param string $interface
     * @param IUObject $object
     * @param IoC $ioc
     * @param IClassable $classable
     */
    public function __construct(string $interface, IUObject $object, IoC $ioc, IClassable $classable)
    {
        $this->interface = $interface;
        $this->object = $object;
        $this->ioc = $ioc;
        $this->classable = $classable;
    }

    /**
     * @return mixed
     * @throws ReflectionException
     */
    public function generate()
    {
        $reflectionClass = new ReflectionClass($this->interface);
        $adapterName = $this->classable->getClassName($reflectionClass);
        if (!class_exists($adapterName)) {
            $eval = "class  $adapterName implements $this->interface { ";
            $eval .= $this->classable->createConstructor();
            foreach ($reflectionClass->getMethods() as $method) {
                $eval .= $this->classable->createMethod($method);
            }
            $eval .= "}; ";
            eval($eval);
        }
        return new $adapterName($this->object, $this->ioc);
    }
}