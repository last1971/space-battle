<?php

namespace Last1971\SpaceBattle\Base;

use Last1971\SpaceBattle\Commands\IoCCommand;
use Last1971\SpaceBattle\Interfaces\IRegistrable;

class IoCRegister implements IRegistrable
{
    public function register(Scope $scope): callable
    {
        return function(...$args) use ($scope) {
            return new IoCCommand(function () use ($args, $scope) {
               if (count($args) > 0 && !in_array($args[0], $scope->reservedWords())) {
                    $scope->put($args[0], $args[1]);
               }
            });
        };
    }
}