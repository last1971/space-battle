<?php

namespace Last1971\SpaceBattle\Base;

use Last1971\SpaceBattle\Interfaces\IUObject;

class UObject implements IUObject
{
    /**
     * @param array $attributes
     */
    public function __construct(private array $attributes = [])
    {
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed
    {
        return $this->attributes[$key];
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set(string $key, mixed $value): void
    {
        $this->attributes[$key] = $value;
    }
}