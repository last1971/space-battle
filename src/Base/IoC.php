<?php

namespace Last1971\SpaceBattle\Base;

class IoC
{
    /**
     * @var Scope
     */
    private Scope $scope;

    /**
     * @param Scope $scope
     */
    public function __construct(Scope $scope)
    {
        $this->scope = $scope;
    }

    /**
     * @param string $class
     * @param ...$args
     * @return mixed
     */
    public function resolve(string $class, ...$args): mixed
    {
        $call = $this->scope->get($class);
        return $call ? $call(...$args) : (class_exists($class) ? new $class(...$args) : null);
    }
}