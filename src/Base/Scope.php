<?php

namespace Last1971\SpaceBattle\Base;

use Last1971\SpaceBattle\Commands\IoCCommand;
use Last1971\SpaceBattle\Interfaces\IRegistrable;
use Illuminate\Support\Collection;

class Scope
{
    /**
     * @var Collection
     */
    private Collection $scopes;

    /**
     * @var string
     */
    private string $currentScope;

    /**
     * @var IRegistrable
     */
    private IRegistrable $registrable;

    public function __construct(IRegistrable $registrable)
    {
        $this->registrable = $registrable;
        $this->scopes = collect();
        $this->currentScope = 'default';
        $this->registerScope($this->currentScope);
    }

    /**
     * @return string[]
     */
    public function reservedWords(): array
    {
        return ['IoCRegister', 'ScopesNew', 'ScopesCurrent', 'ParentScope'];
    }

    /**
     * @param string $class
     * @return callable|null
     */
    /** @suppress PhanNonClassMethodCall */
    public function get(string $class): ?callable
    {
        $currentScope = fn() => $this->currentScope;
        do {
            /** @var Collection $scope */
            $scope = $this->scopes->get($currentScope());
            $res = $scope?->get($class);
            $currentScope = $scope?->get('ParentScope');
        } while (!$res && $currentScope);
        return $res;
    }

    /**
     * @param string $class
     * @param callable $value
     * @return void
     */
    /** @suppress PhanNonClassMethodCall */
    public function put(string $class, callable $value): void
    {
        /** @var Collection $scope */
        $scope = $this->scopes->get($this->currentScope);
        $scope->put($class, $value);
    }

    /**
     * @param string $scopeName
     * @return void
     */
    /** @suppress PhanTypeMismatchArgumentProbablyReal */
    private function registerScope(string $scopeName): void
    {
        if ($scopeName === 'default' && !$this->scopes->has($scopeName)) {
            $this->scopes->put($scopeName, collect([
                'IoCRegister' => $this->registrable->register($this),
                'ParentScope' => fn() => null,
                'ScopesNew' => function(...$args) {
                    return new IoCCommand(function () use ($args) {
                        $this->registerScope($args[0]);
                    });
                },
                'ScopesCurrent' => function(...$args) {
                    return new IoCCommand(function () use ($args) {
                        $this->registerScope($args[0]);
                        $this->currentScope = $args[0];
                    });
                },
            ]));
        } elseif (!$this->scopes->has($scopeName)) {
            $parentScope = $this->currentScope;
            $this->scopes->put($scopeName, collect(['ParentScope' => fn() => $parentScope ]));
        }
    }
}