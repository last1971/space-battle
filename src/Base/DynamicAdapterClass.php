<?php

namespace Last1971\SpaceBattle\Base;

use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Base\Methods\MethodGet;
use Last1971\SpaceBattle\Base\Methods\MethodSet;
use Last1971\SpaceBattle\Base\Methods\MethodVoid;
use Last1971\SpaceBattle\Interfaces\IClassable;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IUObject;
use ReflectionClass;
use ReflectionMethod;
use ReflectionNamedType;

class DynamicAdapterClass implements IClassable
{
    /** @var Collection  */
    private Collection $methods;

    public function __construct()
    {
        $this->methods = collect([
            'void0' => new MethodVoid(),
            'void1' => new MethodSet(),
        ]);
    }

    /**
     * @param ReflectionClass $class
     * @return string
     */
    public function getClassName(ReflectionClass $class): string
    {
        return $class->getShortName() . 'Adapter';
    }

    /**
     * @return string
     */
    public function createConstructor(): string
    {
        $iUObject = IUObject::class;
        $IoC = IoC::class;
        $response = "private $iUObject \$uObject; ";
        $response .= "private $IoC \$ioc; ";
        $response .= "public function __construct($iUObject \$uObject, $IoC \$ioc) ";
        $response .= "{ \$this->uObject = \$uObject; ";
        $response.= "\$this->ioc = \$ioc; } ";
        return $response;
    }

    /**
     * @param ReflectionMethod $method
     * @return string
     */
    /** @suppress PhanUndeclaredInvokeInCallable */
    public function createMethod(ReflectionMethod $method): string
    {
        $className = $this->getClassName($method->getDeclaringClass());
        $parameters = $method->getParameters();
        $returnType = $method->getReturnType();
        $returnTypeName = $returnType instanceof ReflectionNamedType ? $returnType->getName() : '';
        $methodable = $this
            ->methods
            ->get($returnTypeName . count($parameters), new MethodGet());
        return $methodable->createMethod($method, $className);
    }

    /**
     * @param IoC $ioc
     * @return void
     */
    public static function iocRegister(IoC $ioc, string $adpterName = 'Adapter'): void
    {
        /** @var ICommand $command */
        $command = $ioc->resolve('IoCRegister', $adpterName, function (string $class, IUObject $object) use ($ioc) {
            $fabric = new CommandAdapterFabric(
                $class,
                $object,
                $ioc,
                new DynamicAdapterClass()
            );
            return $fabric->generate();
        });
        if ($command instanceof  ICommand) {
            $command->execute();
        }
    }
}