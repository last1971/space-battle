<?php

namespace Last1971\SpaceBattle\Base;

use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Interfaces\IQuadrantMovable;


class Quadrant
{
    /**
     * @var Collection<IQuadrantMovable>
     */
    private Collection $objects;

    /**
     * @param Vector $leftTop
     * @param Vector $rightBottom
     */
    public function __construct(private Vector $leftTop, private Vector $rightBottom)
    {
        $this->objects = collect();
    }

    /**
     * @param Vector $point
     * @return bool
     */
    public function contain(Vector $point): bool
    {
        $leftTop = $this->leftTop->getCoordinates();
        $rightBottom  = $this->rightBottom->getCoordinates();
        $obj = $point->getCoordinates();
        return
            $leftTop[0] <= $obj[0]
            &&
            $leftTop[1] > $obj[1]
            &&
            $rightBottom[0] > $obj[0]
            &&
            $rightBottom[1] <= $obj[1];
    }

    /**
     * @return Collection<IQuadrantMovable>
     */
    public function getObjects(): Collection
    {
        return $this->objects;
    }

    /**
     * @param Collection<IQuadrantMovable> $objects
     * @return void
     */
    public function setObjects(Collection $objects): void
    {
        $this->objects = $objects;
    }
}