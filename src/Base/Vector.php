<?php

namespace Last1971\SpaceBattle\Base;

class Vector
{
    /**
     * @var int[]
     */
    private array $coordinates;

    /**
     * @param int[] $coordinates
     */
    public function __construct(array $coordinates = [])
    {
        $this->setCoordinates($coordinates);
    }

    /**
     * @return int[]
     */
    public function getCoordinates(): array
    {
        return $this->coordinates;
    }

    /**
     * @return array
     */
    public function getCoordinateNames(): array
    {
        $coordinatesName = array_keys($this->coordinates);
        sort($coordinatesName);
        return $coordinatesName;
    }

    /**
     * @param int[] $coordinates
     * @return void
     */
    public function setCoordinates(array $coordinates)
    {
        foreach ($coordinates as $key => $value)
        {
            $this->coordinates[$key] = intval($value);
        }
    }

    /**
     * @param Vector $v1
     * @param Vector $v2
     * @return Vector
     * @throws \Exception
     */

    public static function plus(Vector $v1, Vector $v2): Vector
    {
        if ($v1->getCoordinateNames() !== $v2->getCoordinateNames())
        {
            throw new \Exception('Different coordinate systems');
        }
        $res = [];
        foreach ($v1->getCoordinates() as $key => $value)
        {
            $res[$key] = $value + $v2->getCoordinates()[$key];
        }
        return new Vector($res);
    }
}