<?php

namespace Last1971\SpaceBattle\Base\Methods;

use Last1971\SpaceBattle\Interfaces\IMethodable;
use ReflectionMethod;
use ReflectionNamedType;

class MethodGet implements IMethodable
{

    /**
     * @param ReflectionMethod $method
     * @param ...$args
     * @return string
     */
    public function createMethod(ReflectionMethod $method, ...$args): string
    {
        $methodName = $method->getName();
        $returnType = $method->getReturnType();
        $returnTypeName = $returnType instanceof ReflectionNamedType ? $returnType->getName() : '';
        $response = "public function $methodName(): $returnTypeName";
        $response .= "{ return \$this->ioc->resolve('$args[0]->$methodName', \$this->uObject); }";
        return $response;
    }
}