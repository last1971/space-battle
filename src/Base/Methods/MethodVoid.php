<?php

namespace Last1971\SpaceBattle\Base\Methods;

use Last1971\SpaceBattle\Interfaces\IMethodable;
use ReflectionMethod;

class MethodVoid implements IMethodable
{

    /**
     * @param ReflectionMethod $method
     * @param ...$args
     * @return string
     */
    public function createMethod(ReflectionMethod $method, ...$args): string
    {
        $methodName = $method->getName();
        $response = "public function $methodName(): void ";
        $response .= "{ \$this->ioc->resolve('$args[0]->$methodName', \$this->uObject)->execute(); }";
        return $response;
    }
}