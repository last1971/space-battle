<?php

namespace Last1971\SpaceBattle\Base\Methods;

use Last1971\SpaceBattle\Interfaces\IMethodable;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;

class MethodSet implements IMethodable
{

    /**
     * @param ReflectionMethod $method
     * @param ...$args
     * @return string
     */
    public function createMethod(ReflectionMethod $method, ...$args): string
    {
        $methodName = $method->getName();
        $parameter = $method->getParameters()[0];
        $response = "public function $methodName(";
        $response .= $this->createParameter($parameter);
        $response .= "): void {";
        $parameterName = $parameter->getName();
        $response .= "\$this->ioc->resolve('$args[0]->$methodName', \$this->uObject";
        $response .= ", \$$parameterName)->execute(); }";
        return $response;
    }

    /**
     * @param ReflectionParameter $parameter
     * @return string
     */
    private function createParameter(ReflectionParameter $parameter): string
    {
        $type = $parameter->getType();
        $returnTypeName = $type instanceof ReflectionNamedType ? $type->getName() : '';
        $name = $parameter->getName();
        $response = "$returnTypeName $$name";
        if ($parameter->isDefaultValueAvailable()) {
            $defaultValue = $parameter->getDefaultValue();
            $response .= "=$defaultValue";
        }
        return $response;
    }
}