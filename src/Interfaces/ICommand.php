<?php

namespace Last1971\SpaceBattle\Interfaces;

interface ICommand
{
    /**
     * @return void
     */
    public function execute(): void;
}