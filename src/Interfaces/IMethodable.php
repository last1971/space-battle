<?php

namespace Last1971\SpaceBattle\Interfaces;

use ReflectionMethod;

interface IMethodable
{
    public function createMethod(ReflectionMethod $method, ...$args): string;
}