<?php

namespace Last1971\SpaceBattle\Interfaces;

use Last1971\SpaceBattle\Base\Vector;

interface IMovable
{
    /**
     * @return Vector
     */
     public function getPosition(): Vector;

    /**
     * @return Vector
     */
     public function getVelocity(): Vector;

    /**
     * @param Vector $newPosition
     * @return void
     */
     public function setPosition(Vector $newPosition): void;
}