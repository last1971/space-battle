<?php

namespace Last1971\SpaceBattle\Interfaces;

use Last1971\SpaceBattle\Base\Direction;

interface IRotatable
{
    /**
     * @return Direction
     */
    public function getDirection(): Direction;

    /**
     * @return int
     */
    public function getAngularVelocity(): int;

}