<?php

namespace Last1971\SpaceBattle\Interfaces;

interface IFuelable
{
    public function getFuel(): int;

    public function setFuel(int $fuel): void;

    public function getFuelConsumption(): int;
}