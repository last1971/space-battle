<?php

namespace Last1971\SpaceBattle\Interfaces;

interface ITickable extends ICommand
{
    public function nextTick(): void;

    public function changeNext(ITickable $next);
}