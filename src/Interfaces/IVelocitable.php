<?php

namespace Last1971\SpaceBattle\Interfaces;

use Last1971\SpaceBattle\Base\Direction;
use Last1971\SpaceBattle\Base\Vector;

interface IVelocitable
{
    /**
     * @return Vector
     */
    public function getVelocity(): Vector;

    /**
     * @param Vector $newVelocity
     * @return void
     */
    public function setVelocity(Vector $newVelocity): void;

    /**
     * @return Direction
     */
    public function getDirection(): Direction;
}