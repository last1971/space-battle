<?php

namespace Last1971\SpaceBattle\Interfaces;

interface IInjectable
{
    public function inject(ICommand $inner);
}