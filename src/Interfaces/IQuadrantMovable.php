<?php

namespace Last1971\SpaceBattle\Interfaces;

use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Base\Quadrant;
use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\BridgeCommand;

interface IQuadrantMovable
{
    /**
     * @return Vector
     */
    public function getPosition(): Vector;

    /**
     * @return Collection<Quadrant>
     */
    public function getQuadrants(): Collection;

    /**
     * @return BridgeCommand
     */
    public function getCheckCollisions(): BridgeCommand;
}