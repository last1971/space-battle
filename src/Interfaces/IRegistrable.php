<?php

namespace Last1971\SpaceBattle\Interfaces;

use Last1971\SpaceBattle\Base\Scope;

interface IRegistrable
{
    public function register(Scope $scope): callable;
}