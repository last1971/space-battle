<?php

namespace Last1971\SpaceBattle\Interfaces;

use Last1971\SpaceBattle\Base\IoC;

interface IExpression
{
    public function interpret(IUObject $context, IoC $ioc): ICommand;
}