<?php

namespace Last1971\SpaceBattle\Interfaces;

use Illuminate\Support\Collection;

interface IExceptionHandler
{
    /**
     * @param ICommand $command
     * @param \Exception $exception
     * @param Collection $queue
     * @return void
     */
    public function handle(ICommand $command, \Exception $exception, Collection $queue): void;
}