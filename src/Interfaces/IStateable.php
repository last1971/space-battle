<?php

namespace Last1971\SpaceBattle\Interfaces;

interface IStateable
{
    /**
     * @return ?IStateable
     */
    public function handle(): ?IStateable;
}