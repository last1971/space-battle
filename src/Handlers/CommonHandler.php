<?php

namespace Last1971\SpaceBattle\Handlers;

use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Interfaces\IStateable;

class CommonHandler implements IStateable
{

    /**
     * @var ExceptionHandler
     */
    private ExceptionHandler $handler;

    /**
     * @var Collection
     */
    private Collection $queue;

    /**
     * @var Collection
     */
    private Collection $states;

    /**
     * @param ExceptionHandler $handler
     * @param Collection $queue
     */
    public function __construct(ExceptionHandler $handler, Collection $queue, $states)
    {
        $this->handler = $handler;
        $this->queue = $queue;
        $this->states = $states;
    }

    /**
     * @return IStateable|null
     */
    /** @suppress PhanTypeMismatchArgument, PhanTypeMismatchReturn */
    public function handle(): ?IStateable
    {
        $command = null;
        if ($this->queue->isNotEmpty()) {
            $command = $this->queue->shift();
            try {
                $command->execute();
            }  catch (\Exception $e) {
                $this->handler->obtain($command, $e, $this->queue);
            }
        }
        return $this->states->get(get_class($command), $this);
    }
}