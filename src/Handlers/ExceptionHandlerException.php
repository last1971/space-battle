<?php

namespace Last1971\SpaceBattle\Handlers;

use Exception;
use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExceptionHandler;

class ExceptionHandlerException implements IExceptionHandler
{

    /**
     * @throws Exception
     */
    public function handle(ICommand $command, Exception $exception, Collection $queue): void
    {
        throw $exception;
    }
}