<?php

namespace Last1971\SpaceBattle\Handlers;

use Last1971\SpaceBattle\Commands\RepeatCommand;
use Last1971\SpaceBattle\Commands\RepeatSecondCommand;
use Monolog\Logger;
use function collect;

class ExceptionHandlerRepeatDoubleThanLog extends ExceptionHandlerRepeatThanLog
{
    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        parent::__construct($logger);
        $this->queue = collect([
            RepeatCommand::class => new ExceptionHandlerRepeatSecond(),
            RepeatSecondCommand::class => new ExceptionHandlerLog($logger),
        ]);
    }
}