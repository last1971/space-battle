<?php

namespace Last1971\SpaceBattle\Handlers;

use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExceptionHandler;
use Illuminate\Support\Collection;
use function collect;

class ExceptionHandler implements IExceptionHandler
{
    /**
     * @var Collection
     */
    private Collection $exceptionHandlers;

    public function __construct()
    {
        $this->exceptionHandlers = collect();
    }

    /**
     * @param string $command
     * @param string $exception
     * @param IExceptionHandler $exceptionHandler
     * @return void
     */
    public function addAction(string $command, string $exception, IExceptionHandler $exceptionHandler)
    {
        $this->exceptionHandlers->put(md5($command . $exception), $exceptionHandler);
    }

    /**
     * @param ICommand $command
     * @param \Exception $exception
     * @param Collection $queue
     * @return void
     */
    public function obtain(ICommand $command, \Exception $exception, Collection $queue): void
    {
        $hash = md5(get_class($command) . get_class($exception));
        $this->exceptionHandlers->get($hash, $this)->handle($command, $exception, $queue);
    }

    /**
     * @param ICommand $command
     * @param \Exception $exception
     * @param Collection $queue
     * @return void
     */
    public function handle(ICommand $command, \Exception $exception, Collection $queue): void
    {
        // do nothing
    }
}