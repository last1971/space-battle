<?php

namespace Last1971\SpaceBattle\Handlers;

use Last1971\SpaceBattle\Commands\RepeatCommand;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExceptionHandler;
use Monolog\Logger;
use Illuminate\Support\Collection;
use function collect;

class ExceptionHandlerRepeatThanLog implements IExceptionHandler
{
    /**
     * @var Collection
     */
    protected Collection $queue;

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->queue = collect([
            RepeatCommand::class => new ExceptionHandlerLog($logger)
        ]);
    }

    /**
     * @param ICommand $command
     * @param \Exception $exception
     * @param Collection $queue
     * @return void
     */
    /** @suppress PhanUndeclaredInvokeInCallable */
    public function handle(ICommand $command, \Exception $exception, Collection $queue): void
    {
        $this
            ->queue
            ->get(get_class($command), new ExceptionHandlerRepeat())
            ->handle($command, $exception, $queue);
    }
}