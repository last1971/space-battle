<?php

namespace Last1971\SpaceBattle\Handlers;

use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Interfaces\IStateable;

class MoveToHandler implements IStateable
{

    /**
     * @var Collection
     */
    private Collection $queue;

    /**
     * @var Collection
     */
    private Collection $states;

    /**
     * @var Collection
     */
    private Collection $newQueue;

    /**
     * @param Collection $queue
     * @param Collection $states
     * @param Collection $newQueue
     */
    public function __construct(Collection $queue, Collection $states, Collection $newQueue)
    {
        $this->queue = $queue;
        $this->states = $states;
        $this->newQueue = $newQueue;
    }

    /**
     * @return IStateable|null
     */
    /** @suppress PhanTypeMismatchReturn */
    public function handle(): ?IStateable
    {
        $command = null;
        if ($this->queue->isNotEmpty()) {
            $command = $this->queue->shift();
            $this->newQueue->push($command);
        }
        return $this->states->get(get_class($command), $this);
    }
}