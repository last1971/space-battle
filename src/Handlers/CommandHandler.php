<?php

namespace Last1971\SpaceBattle\Handlers;

use Illuminate\Support\Collection;

class CommandHandler
{
    /**
     * @var ExceptionHandler
     */
    private ExceptionHandler $handler;

    /**
     * @var Collection
     */
    private Collection $queue;

    /**
     * @param ExceptionHandler $handler
     * @param Collection $queue
     */
    public function __construct(ExceptionHandler $handler, Collection $queue)
    {
        $this->handler = $handler;
        $this->queue = $queue;
    }

    /**
     * @return void
     */
    /** @suppress PhanTypeMismatchArgument */
    public function handle(): void
    {
       if ($this->isNotEmptyQueue()) {
           $command = $this->queue->shift();
           try {
               $command->execute();
           }  catch (\Exception $e) {
               $this->handler->obtain($command, $e, $this->queue);
           }
       }
    }

    /**
     * @return bool
     */
    public function isNotEmptyQueue(): bool
    {
        return $this->queue->isNotEmpty();
    }
}