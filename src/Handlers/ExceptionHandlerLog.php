<?php

namespace Last1971\SpaceBattle\Handlers;

use Last1971\SpaceBattle\Commands\LogCommand;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExceptionHandler;
use Monolog\Logger;
use Illuminate\Support\Collection;

class ExceptionHandlerLog implements IExceptionHandler
{
    /**
     * @var Logger
     */
    private Logger $logger;

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param ICommand $command
     * @param \Exception $exception
     * @param Collection $queue
     * @return void
     */
    public function handle(ICommand $command, \Exception $exception, Collection $queue): void
    {
         $queue->push(new LogCommand($command, $exception, $this->logger));
    }
}