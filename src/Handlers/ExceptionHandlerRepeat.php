<?php

namespace Last1971\SpaceBattle\Handlers;

use Last1971\SpaceBattle\Commands\RepeatCommand;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExceptionHandler;
use Illuminate\Support\Collection;

class ExceptionHandlerRepeat implements IExceptionHandler
{

    /**
     * @param ICommand $command
     * @param \Exception $exception
     * @param Collection $queue
     * @return void
     */
    public function handle(ICommand $command, \Exception $exception, Collection $queue): void
    {
        $queue->push(new RepeatCommand($command));
    }
}