<?php

namespace Last1971\SpaceBattle\Commands;

use Exception;
use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IVelocitable;

class ChangeVelocity implements ICommand
{
    /**
     * @var IVelocitable
     */
    private IVelocitable $velocitable;

    /**
     * @param IVelocitable $velocitable
     */
    public function __construct(IVelocitable $velocitable)
    {
        $this->velocitable = $velocitable;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function execute(): void
    {
        $oldCoordinates = $this->velocitable->getVelocity()->getCoordinates();
        $keys = $this->velocitable->getVelocity()->getCoordinateNames();
        $XKey = in_array('x', $keys, true) ? 'x' : 0;
        $YKey = in_array('y', $keys, true) ? 'y' : 1;
        $direction = $this->velocitable->getDirection();
        $velocity = sqrt($oldCoordinates[$XKey] * $oldCoordinates[$XKey] + $oldCoordinates[$YKey] * $oldCoordinates[$YKey]);
        $this->velocitable->setVelocity(
            new Vector([
                $XKey =>
                    intval(
                        $velocity
                        *
                        round(cos(2 * pi() * $direction->getDirection() / $direction->getDirectionsNumber()))
                    ),
                $YKey =>
                    intval(
                        $velocity
                        *
                        round(sin(2 * pi() * $direction->getDirection() / $direction->getDirectionsNumber()))
                    ),
            ])
        );
    }
}