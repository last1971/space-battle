<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\DTOs\CommandMessageDTO;
use Last1971\SpaceBattle\Interfaces\ICommand;

class InterpretCommand implements ICommand
{
    /**
     * @param IoC $ioc
     * @param CommandMessageDTO $messageDTO
     */
    public function __construct(
        private readonly IoC $ioc,
        private readonly CommandMessageDTO $messageDTO,
    )
    {

    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $command = $this->ioc->resolve('AllowedCommands', $this->messageDTO);
        if (is_callable($command)) {
            $command($this->messageDTO);
        }
    }
}