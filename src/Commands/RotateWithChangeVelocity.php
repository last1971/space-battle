<?php

namespace Last1971\SpaceBattle\Commands;

class RotateWithChangeVelocity extends MacroCommand
{
    public function __construct(Rotate $rotate, ChangeVelocity $changeVelocity)
    {
        parent::__construct([$rotate, $changeVelocity]);
    }

}