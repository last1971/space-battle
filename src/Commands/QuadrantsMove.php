<?php

namespace Last1971\SpaceBattle\Commands;

use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Interfaces\IQuadrantMovable;

class QuadrantsMove implements \Last1971\SpaceBattle\Interfaces\ICommand
{
    public function __construct(private IQuadrantMovable $quadrantMovable, private IoC $ioc)
    {
    }

    /**
     * @inheritDoc
     */
    public function execute(): void
    {
        /** @var Collection $quadrants */
        $quadrants = $this->ioc->resolve('quadrants');
        $oldQuadrants = clone $this->quadrantMovable->getQuadrants();
        foreach ($oldQuadrants->keys() as $quadrantName) {
            $command = new QuadrantMove($this->quadrantMovable, $quadrantName, $quadrants->get($quadrantName));
            $command->execute();
        }
        $newQuadrants = $this->quadrantMovable->getQuadrants();
        if ($oldQuadrants !== $newQuadrants) {
            $this->quadrantMovable
                ->getCheckCollisions()
                ->inject(
                    new MacroCommand(
                        $newQuadrants
                            ->map(fn($newQuadrant) => $newQuadrant
                                ->getObjects()
                                ->map(fn($obj) => new CheckCollision($this->quadrantMovable, $obj))
                            )
                            ->collapse()
                            ->unique()
                            ->toArray()
                    )
                );
        }
    }
}