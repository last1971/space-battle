<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\ITickable;

class ChangeTick implements ICommand
{
    /**
     * @var ITickable
     */
    private ITickable $oldTick;

    /**
     * @var ITickable
     */
    private ITickable $newTick;

    /**
     * @param ITickable $oldTick
     * @param ITickable $newTick
     */
    public function __construct(ITickable $oldTick, ITickable $newTick)
    {
        $this->oldTick = $oldTick;
        $this->newTick = $newTick;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->oldTick->changeNext($this->newTick);
    }
}