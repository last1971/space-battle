<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Handlers\CommandHandler;
use Last1971\SpaceBattle\Interfaces\ITickable;

class Tick implements ITickable
{
    /**
     * @var ITickable|Tick
     */
    protected ITickable $next;

    /**
     * @var CommandHandler
     */
    protected CommandHandler $handler;

    /**
     * @param CommandHandler $handler
     */
    public function __construct(CommandHandler $handler)
    {
        $this->handler = $handler;
        $this->next = $this;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->handler->handle();
        $this->nextTick();
    }

    /**
     * @return void
     */
    public function nextTick(): void
    {
        $this->next->execute();
    }

    /**
     * @param ITickable $next
     * @return void
     */
    public function changeNext(ITickable $next)
    {
        $this->next = $next;
    }
}