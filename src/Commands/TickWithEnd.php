<?php

namespace Last1971\SpaceBattle\Commands;

class TickWithEnd extends Tick
{
    /**
     * @return void
     */
    public function nextTick(): void
    {
        if ($this->handler->isNotEmptyQueue()) {
            $this->next->execute();
        }
    }
}