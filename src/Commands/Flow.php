<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IInjectable;

class Flow implements ICommand, IInjectable
{
    const START = 1;
    const STOP = 2;

    /**
     * @var int
     */
    private int $state;

    /**
     * @var ICommand
     */
    private ICommand $inner;

    /**
     * @param ICommand $inner
     */
    public function __construct(ICommand $inner)
    {
        $this->state = $this::STOP;
        $this->inject($inner);
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->state = $this::START;
        $this->inner->execute();
        $this->state = $this::STOP;
    }

    /**
     * @param ICommand $inner
     * @return void
     */
    public function inject(ICommand $inner)
    {
        $this->inner = $inner;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }
}