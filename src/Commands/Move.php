<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IMovable;

class Move implements ICommand
{
    /**
     * @var IMovable
     */
    private IMovable $movable;

    /**
     * @param IMovable $movable
     */
    public function __construct(IMovable $movable)
    {
        $this->movable = $movable;
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function execute(): void
    {
        $this->movable->setPosition(Vector::plus($this->movable->getPosition(), $this->movable->getVelocity()));
    }
}