<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IUObject;

class Fire implements ICommand
{
    /**
     * @param IUObject $object
     */
    public function __construct(private IUObject $object)
    {
    }

    public function execute(): void
    {

    }
}