<?php

namespace Last1971\SpaceBattle\Commands;

class MoveWithBurnFuel extends MacroCommand
{
    public function __construct(CheckFuel $checkFuel, Move $move, BurnFuel $burnFuel)
    {
        parent::__construct(compact('checkFuel', 'move', 'burnFuel'));
    }
}