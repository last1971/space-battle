<?php

namespace Last1971\SpaceBattle\Commands;

use InvalidArgumentException;
use Last1971\SpaceBattle\Interfaces\ICommand;

class MacroCommand implements \Last1971\SpaceBattle\Interfaces\ICommand
{
    /**
     * @var ICommand[]
     */
    private array $commands;

    public function __construct(array $commands)
    {
        foreach ($commands as $command) {
            if (!($command instanceof ICommand)) {
                throw new InvalidArgumentException('Must be array of Commands');
            }
        }
        $this->commands = $commands;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        foreach ($this->commands as $command) {
            $command->execute();
        }
    }
}