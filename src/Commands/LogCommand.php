<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ICommand;
use Monolog\Logger;

class LogCommand implements ICommand
{
    /**
     * @var ICommand
     */
    private ICommand $command;

    /**
     * @var \Exception
     */
    private \Exception $exception;

    /**
     * @var Logger
     */
    private Logger $logger;

    public function __construct(ICommand $command, \Exception $exception, Logger $logger)
    {
        $this->logger = $logger;
        $this->command = $command;
        $this->exception = $exception;
//       $this->logger = new Logger('log');
//        $this->logger->pushHandler(new StreamHandler(__DIR__.'/../errors.log', Logger::ERROR));
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->logger->error(
            'An error has occurred',
            [
                'message' => $this->exception->getMessage(),
                'exception' => get_class($this->exception),
                'command' => get_class($this->command),
            ],
        );
    }
}