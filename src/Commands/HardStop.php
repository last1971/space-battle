<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\ITickable;

class HardStop implements ICommand
{
    /**
     * @var ITickable
     */
    private ITickable $oldTick;

    /**
     * @var TickBroken
     */
    private TickBroken $newTick;

    /**
     * @var ChangeTick
     */
    private ChangeTick $changeTick;

    /**
     * @param ITickable $oldTick
     * @param TickBroken $newTick
     */
    public function __construct(ITickable $oldTick, TickBroken $newTick)
    {
        $this->changeTick = new ChangeTick($oldTick, $newTick);
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->changeTick->execute();
    }
}