<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ICommand;

class RunCommand implements ICommand
{

    /**
     * @inheritDoc
     */
    public function execute(): void
    {
    }
}