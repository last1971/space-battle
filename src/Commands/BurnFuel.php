<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\IFuelable;

class BurnFuel implements \Last1971\SpaceBattle\Interfaces\ICommand
{

    /**
     * @var IFuelable
     */
    private IFuelable $fuelable;

    /**
     * @param IFuelable $fuelable
     */
    public function __construct(IFuelable $fuelable)
    {
        $this->fuelable = $fuelable;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->fuelable->setFuel($this->fuelable->getFuel() - $this->fuelable->getFuelConsumption());
    }
}