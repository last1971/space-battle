<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IQuadrantMovable;

class CheckCollision implements ICommand
{
    public function __construct(private IQuadrantMovable $object1, private IQuadrantMovable $object2)
    {
    }

    public function execute(): void
    {
        $this->object2->getPosition();
        $this->object1->getPosition();
    }
}