<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\IUObject;

class Stop implements \Last1971\SpaceBattle\Interfaces\ICommand
{

    /**
     * @param IUObject $object
     */
    public function __construct(private IUObject $object)
    {

    }

    /**
     * @return void
     */
    public function execute(): void
    {

    }
}