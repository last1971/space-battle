<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ITickable;

class SoftStop implements \Last1971\SpaceBattle\Interfaces\ICommand
{
    /**
     * @var ITickable
     */
    private ITickable $oldTick;

    /**
     * @var TickWithEnd
     */
    private TickWithEnd $newTick;

    /**
     * @var ChangeTick
     */
    private ChangeTick $changeTick;

    /**
     * @param ITickable $oldTick
     * @param TickWithEnd $newTick
     */
    public function __construct(ITickable $oldTick, TickWithEnd $newTick)
    {
        $this->changeTick = new ChangeTick($oldTick, $newTick);
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->changeTick->execute();
    }
}