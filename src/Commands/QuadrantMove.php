<?php

namespace Last1971\SpaceBattle\Commands;

use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Base\Quadrant;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IQuadrantMovable;

class QuadrantMove implements ICommand
{
    /**
     * @param IQuadrantMovable $movable
     * @param string $quadrantName
     * @param Collection<Quadrant> $quadrants
     */
    public function __construct(
        private readonly IQuadrantMovable $movable,
        private readonly string $quadrantName,
        private readonly Collection $quadrants
    )
    {
    }

    /**
     * @return void
     */
    /** @suppress PhanTypeMismatchArgumentProbablyReal */
    public function execute(): void
    {
        $position = $this->movable->getPosition();
        $objectQuadrants = $this->movable->getQuadrants();
        $quadrant = $objectQuadrants->get($this->quadrantName);
        if ($quadrant && $quadrant->contain($position)) return;
        $quadrant->setObjects($quadrant->getObjects()->reject(fn($v) => $v !== $this->movable));
        foreach ($this->quadrants as $quadrant) {
            if ($quadrant->contain($position)) {
                $objectQuadrants->put($this->quadrantName, $quadrant);
                $quadrant->getObjects()->push($this->movable);
                break;
            }
        }
    }
}