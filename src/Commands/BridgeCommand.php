<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ICommand;

class BridgeCommand implements ICommand
{
    /**
     * @param ICommand $internal
     */
    public function __construct(private ICommand $internal)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->internal->execute();
    }

    /**
     * @param ICommand $internal
     * @return void
     */
    public function inject(ICommand $internal): void
    {
        $this->internal = $internal;
    }
}