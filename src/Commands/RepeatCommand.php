<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ICommand;

class RepeatCommand implements ICommand
{
    /**
     * @var ICommand
     */
    private ICommand $command;

    /**
     * @param ICommand $command
     */
    public function __construct(ICommand $command)
    {
        $this->command = $command;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->command->execute();
    }
}