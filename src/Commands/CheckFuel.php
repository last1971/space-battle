<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Exceptions\CommandException;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IFuelable;

class CheckFuel implements ICommand
{
    /**
     * @var IFuelable
     */
    private IFuelable $fuelable;

    /**
     * @param IFuelable $fuelable
     */
    public function __construct(IFuelable $fuelable)
    {
        $this->fuelable = $fuelable;
    }

    /**
     * @return void
     * @throws CommandException
     */

    public function execute(): void
    {
        if ($this->fuelable->getFuel() - $this->fuelable->getFuelConsumption() < 0) {
            throw new CommandException($this, 'Not enough fuel');
        }
    }
}