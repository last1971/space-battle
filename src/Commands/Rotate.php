<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IRotatable;

class Rotate implements ICommand
{
    /**
     * @var IRotatable
     */
    private IRotatable $rotatable;

    /**
     * @param IRotatable $rotatable
     */
    public function __construct(IRotatable $rotatable)
    {
        $this->rotatable = $rotatable;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->rotatable->getDirection()->next($this->rotatable->getAngularVelocity());
    }
}