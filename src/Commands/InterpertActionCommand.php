<?php

namespace Last1971\SpaceBattle\Commands;

use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Expressions\FireExpression;
use Last1971\SpaceBattle\Expressions\StartMove;
use Last1971\SpaceBattle\Expressions\StopMove;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExpression;
use Last1971\SpaceBattle\Interfaces\IUObject;

class InterpertActionCommand implements ICommand
{

    private $actions = [
        'StartMove' => StartMove::class,
        'StopMove' => StopMove::class,
        'Fire' => FireExpression::class,
    ];

    public function __construct(private IUObject $context, private IoC $ioc)
    {

    }

    /** @suppress PhanTypeMismatchDimAssignment,PhanTypeMismatchDimFetch*/
    public function execute(): void
    {
        /** @var string $action */
        $action = $this->context->get('action');
        if (!isset($this->actions[$action])) return;
        /** @var IExpression $expression */
        $expression = new $this->actions[$action]();
        $expression->interpret($this->context, $this->ioc)->execute();
    }
}