<?php

namespace Tests;

use Last1971\SpaceBattle\Base\Direction;
use PHPUnit\Framework\TestCase;

class DirectionTest extends TestCase
{
    /**
     * @var Direction
     */
    private Direction $direction;

    private int $angularVelocity;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->direction = new Direction(1, 8);
        $this->angularVelocity = 3;
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->direction);
        unset($this->angularVelocity);
    }

    /**
     * @return void
     */
    public function testNext(): void
    {
        $this->direction->next($this->angularVelocity);
        $this->assertEquals(4, $this->direction->getDirection());
    }

    /**
     * @return void
     */
    public function testNextException(): void
    {
        $this->expectException(\TypeError::class);
        $this->direction->next('A');
    }
}