<?php

namespace Tests;

use Exception;
use Last1971\SpaceBattle\Base\Direction;
use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\ChangeVelocity;
use Last1971\SpaceBattle\Commands\Rotate;
use Last1971\SpaceBattle\Commands\RotateWithChangeVelocity;
use Last1971\SpaceBattle\Interfaces\IRotatable;
use Last1971\SpaceBattle\Interfaces\IVelocitable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RotateWithChangeVelocityTest extends TestCase
{
    /**
     * @var RotateWithChangeVelocity
     */
    private RotateWithChangeVelocity $macroCommand;

    /**
     * @var IVelocitable|MockObject
     */
    private IVelocitable $velocitable;

    /**
     * @var IRotatable|MockObject
     */
    private IRotatable $rotatable;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->velocitable = $this->createMock(IVelocitable::class);
        $this->rotatable = $this->createMock(IRotatable::class);
        $this->macroCommand = new RotateWithChangeVelocity(
            new Rotate($this->rotatable),
            new ChangeVelocity($this->velocitable)
        );
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->macroCommand);
        unset($this->velocitable);
        unset($this->rotatable);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testExecute()
    {
        $direction = new Direction(2, 8);
        $this->rotatable->expects($this->once())->method('getAngularVelocity')->willReturn(2);
        $this->rotatable->expects($this->once())->method('getDirection')->willReturn($direction);
        $this->velocitable->expects($this->once())->method('getDirection')->willReturn($direction);
        $this->velocitable->expects($this->exactly(2))->method('getVelocity')->willReturn(new Vector([0, 2]));
        $this->velocitable->method('setVelocity')->willReturnCallback(function (Vector $newVelocity) {
            $this->assertEquals(new Vector([-2, 0]), $newVelocity);
        });
        $this->macroCommand->execute();
    }
}