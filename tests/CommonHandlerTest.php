<?php

namespace Tests;

use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Commands\HardStopCommand;
use Last1971\SpaceBattle\Commands\MoveToCommand;
use Last1971\SpaceBattle\Handlers\CommonHandler;
use Last1971\SpaceBattle\Handlers\ExceptionHandler;
use Last1971\SpaceBattle\Handlers\MoveToHandler;
use Last1971\SpaceBattle\Interfaces\ICommand;
use PHPUnit\Framework\TestCase;

class CommonHandlerTest extends TestCase
{
    /**
     * @var Collection
     */
    private Collection $queue;

    /**
     * @var Collection
     */
    private Collection $states;

    /**
     * @var ExceptionHandler
     */
    private ExceptionHandler $exceptionHandler;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->queue = collect();
        $this->exceptionHandler = new ExceptionHandler();
        $this->states = collect([
            HardStopCommand::class => null,
            MoveToCommand::class => new MoveToHandler($this->queue, collect(), collect()),
        ]);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->queue);
        unset($this->exceptionHandler);
        unset($this->states);
    }

    /**
     * @return void
     */
    public function testHandle(): void
    {
        $command = $this->createMock(ICommand::class);
        $command->expects($this->once())->method('execute');
        $this->queue->push($command);
        $handler = new CommonHandler($this->exceptionHandler, $this->queue, $this->states);
        $nextHandler = $handler->handle();
        $this->assertEmpty($this->queue);
        $this->assertEquals($handler, $nextHandler);
    }

    /**
     * @return void
     */
    public function testHardStop(): void
    {
        $command = new HardStopCommand();
        $this->queue->push($command);
        $handler = new CommonHandler($this->exceptionHandler, $this->queue, $this->states);
        $nextHandler = $handler->handle();
        $this->assertEmpty($this->queue);
        $this->assertEquals(null, $nextHandler);
    }

    /**
     * @return void
     */
    public function testMoveTo(): void
    {
        $command = new MoveToCommand();
        $this->queue->push($command);
        $handler = new CommonHandler($this->exceptionHandler, $this->queue, $this->states);
        $nextHandler = $handler->handle();
        $this->assertEmpty($this->queue);
        $this->assertEquals(MoveToHandler::class, get_class($nextHandler));
    }

}