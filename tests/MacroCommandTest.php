<?php

namespace Tests;

use InvalidArgumentException;
use Last1971\SpaceBattle\Commands\MacroCommand;
use Last1971\SpaceBattle\Commands\Move;
use Last1971\SpaceBattle\Commands\Rotate;
use PHPUnit\Framework\TestCase;

class MacroCommandTest extends TestCase
{
    /**
     * @return void
     */
    public function testConstructException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new MacroCommand([1, 2, 3]);
    }
}