<?php

namespace Tests;

use Last1971\SpaceBattle\Base\Quadrant;
use Last1971\SpaceBattle\Base\Vector;
use PHPUnit\Framework\TestCase;

class QuadrantTest extends TestCase
{
    /**
     * @return void
     */
    public function testContain(): void
    {
        $quadrant = new Quadrant(new Vector([0,10]), new Vector([10, 0]));
        $point1 = new Vector([5, 5]);
        $point2 = new Vector([10, 0]);
        $this->assertTrue($quadrant->contain($point1));
        $this->assertFalse($quadrant->contain($point2));
    }
}