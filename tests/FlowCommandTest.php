<?php

namespace Tests;

use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Base\IoCRegister;
use Last1971\SpaceBattle\Base\Scope;
use Last1971\SpaceBattle\Commands\Flow;
use Last1971\SpaceBattle\Interfaces\ICommand;
use PHPUnit\Framework\TestCase;

class FlowCommandTest extends TestCase
{
    /**
     * @var IoC
     */
    private IoC $ioc;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $registrable = new IoCRegister();
        $scope = new Scope($registrable);
        $this->ioc = new IoC($scope);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->ioc);
    }

    /**
     * @return void
     */
    public function testStartStop(): void
    {
        $command = $this->createMock(ICommand::class);
        $command->method('execute')->willReturnCallback(function () {
            $this->assertEquals(Flow::START, $this->ioc->resolve('FlowCommand')->getState());
        });
        $flowCommand = new Flow($command);
        $this->ioc->resolve('IoCRegister', 'FlowCommand', function () use ($flowCommand) {
            return $flowCommand;
        })->execute();
        $flowCommand->execute();
        $this->assertEquals(Flow::STOP, $flowCommand->getState());
    }
}