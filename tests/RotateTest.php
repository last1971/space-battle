<?php

namespace Tests;

use Last1971\SpaceBattle\Base\Direction;
use Last1971\SpaceBattle\Interfaces\IRotatable;
use PHPUnit\Framework\MockObject\IncompatibleReturnValueException;
use PHPUnit\Framework\TestCase;

class RotateTest extends TestCase
{
    private $rotatable;

    private Direction $direction;

    /**
     * @return void
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->rotatable = $this->createMock(IRotatable::class);
        $this->direction = new Direction(5, 8);
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->rotatable);
        unset($this->direction);
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testGetDirection(): void
    {
        $this->rotatable->method('getDirection')->willReturn($this->direction);
        $this->assertEquals(new Direction(5, 8), $this->rotatable->getDirection());
    }

    /**
     * @return void
     */
    public function testGetDirectionError(): void
    {
        $this->expectException(IncompatibleReturnValueException::class);
        $this->rotatable->method('getDirection')->willReturn([[5, 8]]);
        $this->rotatable->getDirection();
    }

    /**
     * @return void
     */
    public function testGetDirectionException(): void
    {
        $this->expectException(\Exception::class);
        $this->rotatable->method('getDirection')->will($this->throwException(new \Exception()));
        $this->rotatable->getDirection();
    }

    /**
     * @return void
     */
    public function testGetAngularVelocity(): void
    {
        $this->rotatable->method('getAngularVelocity')->willReturn(1);
        $this->assertEquals(1, $this->rotatable->getAngularVelocity());
    }

    /**
     * @return void
     */
    public function testGetAngularVelocityError(): void
    {
        $this->expectException(IncompatibleReturnValueException::class);
        $this->rotatable->method('getAngularVelocity')->willReturn('A');
        $this->rotatable->getAngularVelocity();
    }

    /**
     * @return void
     */
    public function testGetAngularVelocityException(): void
    {
        $this->expectException(\Exception::class);
        $this->rotatable->method('getAngularVelocity')->will($this->throwException(new \Exception()));
        $this->rotatable->getAngularVelocity();
    }

}