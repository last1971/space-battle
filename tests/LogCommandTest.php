<?php

namespace Tests;

use Last1971\SpaceBattle\Commands\LogCommand;
use Last1971\SpaceBattle\Commands\Move;
use Monolog\Logger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class LogCommandTest extends TestCase
{
    /**
     * @var LogCommand
     */
    private LogCommand $logCommand;

    /**
     * @var Logger|MockObject
     */
    private Logger $logger;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $command = $this->createMock(Move::class);
        $exception = $this->createMock(\Exception::class);
        $this->logger = $this->createMock(Logger::class);
        $this->logCommand = new LogCommand($command, $exception, $this->logger);
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->logCommand);
        unset($this->logger);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $this->logger->method('error')->willReturnCallback(function (string $message, array $context) {
            $this->assertEquals('An error has occurred', $message);
            $this->assertIsArray($context);
            $this->assertArrayHasKey('exception', $context);
            $this->assertArrayHasKey('message', $context);
            $this->assertArrayHasKey('command', $context);
        });
        $this->logCommand->execute();
    }
}