<?php

namespace Tests;

use Last1971\SpaceBattle\Base\DynamicAdapterClass;
use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Base\IoCRegister;
use Last1971\SpaceBattle\Base\Quadrant;
use Last1971\SpaceBattle\Base\Scope;
use Last1971\SpaceBattle\Base\UObject;
use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\BridgeCommand;
use Last1971\SpaceBattle\Commands\IoCCommand;
use Last1971\SpaceBattle\Commands\MacroCommand;
use Last1971\SpaceBattle\Commands\QuadrantMove;
use Last1971\SpaceBattle\Commands\QuadrantsMove;
use Last1971\SpaceBattle\Interfaces\IQuadrantMovable;
use Last1971\SpaceBattle\Interfaces\IUObject;

class QuadrantMoveTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var IQuadrantMovable|mixed
     */
    private IQuadrantMovable $movable1;

    /**
     * @var IoC
     */
    private IoC $ioc;

    /**
     * @var Quadrant
     */
    private Quadrant $quadrant1;

    /**
     * @var Quadrant
     */
    private Quadrant $quadrant2;

    /**
     * @var Quadrant
     */
    private Quadrant $quadrant3;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $registrable = new IoCRegister();
        $scope = new Scope($registrable);
        $this->ioc = new IoC($scope);
        $this->quadrant1 = new Quadrant(new Vector([0, 10]), new Vector([10, 0]));
        $this->quadrant2 = new Quadrant(new Vector([10, 10]), new Vector([20, 0]));
        $this->quadrant3 = new Quadrant(new Vector([5, 15]), new Vector([15, 5]));
        $this->ioc->resolve(
            'IoCRegister',
            'IQuadrantMovableAdapter->getPosition',
            function (IUObject $object) {
                return $object->get('Position');
            },
        )->execute();
        $this->ioc->resolve(
            'IoCRegister',
            'IQuadrantMovableAdapter->getQuadrants',
            function (IUObject $object) {
                return $object->get('Quadrants');
            },
        )->execute();
        DynamicAdapterClass::iocRegister($this->ioc);
        $obj1 = new UObject([
            'Position' => new Vector([14, 6]),
            'Quadrants' => collect(['first' => $this->quadrant1, 'second' => $this->quadrant3]),
        ]);
        $obj2 = $this->createMock(IUObject::class);
        $obj3 = $this->createMock(IUObject::class);
        $obj4 = $this->createMock(IUObject::class);
        $obj5 = $this->createMock(IUObject::class);
        $this->movable1 = $this->ioc->resolve('Adapter', IQuadrantMovable::class, $obj1);
        $movable2 = $this->ioc->resolve('Adapter', IQuadrantMovable::class, $obj2);
        $movable3 = $this->ioc->resolve('Adapter', IQuadrantMovable::class, $obj3);
        $movable4 = $this->ioc->resolve('Adapter', IQuadrantMovable::class, $obj4);
        $movable5 = $this->ioc->resolve('Adapter', IQuadrantMovable::class, $obj5);
        $this->quadrant1->setObjects(collect([$this->movable1, $movable2]));
        $this->quadrant2->setObjects(collect([$movable3, $movable4, $movable5]));
        $this->quadrant3->setObjects(collect([$this->movable1, $movable3, $movable4, $movable5]));
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->movable1);
        unset($this->ioc);
        unset($this->quadrant1);
        unset($this->quadrant2);
        unset($this->quadrant3);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $first = collect([$this->quadrant1, $this->quadrant2]);
        $second  = collect([$this->quadrant3]);
        $command = new QuadrantMove($this->movable1, 'first', $first);
        $command->execute();
        $this->assertEquals(1, $this->quadrant1->getObjects()->count());
        $this->assertEquals(4, $this->quadrant2->getObjects()->count());
        $this->assertEquals(4, $this->quadrant3->getObjects()->count());
        $command = new QuadrantMove($this->movable1, 'second', $second);
        $command->execute();
        $this->assertEquals(1, $this->quadrant1->getObjects()->count());
        $this->assertEquals(4, $this->quadrant2->getObjects()->count());
        $this->assertEquals(4, $this->quadrant3->getObjects()->count());
        $this->assertEquals(
            collect(['first' => $this->quadrant2, 'second' => $this->quadrant3]), $this->movable1->getQuadrants()
        );
    }
}