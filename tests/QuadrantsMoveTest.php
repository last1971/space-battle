<?php

namespace Tests;

use Last1971\SpaceBattle\Base\DynamicAdapterClass;
use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Base\IoCRegister;
use Last1971\SpaceBattle\Base\Quadrant;
use Last1971\SpaceBattle\Base\Scope;
use Last1971\SpaceBattle\Base\UObject;
use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\BridgeCommand;
use Last1971\SpaceBattle\Commands\IoCCommand;
use Last1971\SpaceBattle\Commands\MacroCommand;
use Last1971\SpaceBattle\Commands\QuadrantsMove;
use Last1971\SpaceBattle\Interfaces\IQuadrantMovable;
use Last1971\SpaceBattle\Interfaces\IUObject;
use PHPUnit\Framework\TestCase;
use function Symfony\Component\Translation\t;

class QuadrantsMoveTest extends TestCase
{
    /**
     * @var IQuadrantMovable|mixed
     */
    private IQuadrantMovable $movable1;

    /**
     * @var IoC
     */
    private IoC $ioc;

    /**
     * @var Quadrant
     */
    private Quadrant $quadrant1;

    /**
     * @var Quadrant
     */
    private Quadrant $quadrant2;

    /**
     * @var Quadrant
     */
    private Quadrant $quadrant3;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $registrable = new IoCRegister();
        $scope = new Scope($registrable);
        $this->ioc = new IoC($scope);
        $this->quadrant1 = new Quadrant(new Vector([0, 10]), new Vector([10, 0]));
        $this->quadrant2 = new Quadrant(new Vector([10, 10]), new Vector([20, 0]));
        $this->quadrant3 = new Quadrant(new Vector([5, 15]), new Vector([15, 5]));
        $this->ioc->resolve(
            'IoCRegister',
            'IQuadrantMovableAdapter->getPosition',
            function (IUObject $object) {
                return $object->get('Position');
            },
        )->execute();
        $this->ioc->resolve(
            'IoCRegister',
            'IQuadrantMovableAdapter->getQuadrants',
            function (IUObject $object) {
                return $object->get('Quadrants');
            },
        )->execute();
        $this->ioc->resolve(
            'IoCRegister',
            'IQuadrantMovableAdapter->getCheckCollisions',
            function (IUObject $object) {
                return $object->get('CheckCollisions');
            },
        )->execute();
        /**
        $this->ioc->resolve(
            'IoCRegister',
            'IQuadrantMovableAdapter->setQuadrant',
            function (IUObject $object, Quadrant $newQuadrant) {
                return new IoCCommand(function () use ($object, $newQuadrant){
                    $object->set('Quadrant', $newQuadrant);
                });
            },
        )->execute();*/
        DynamicAdapterClass::iocRegister($this->ioc);
        $obj1 = new UObject([
            'Position' => new Vector([14, 6]),
            'Quadrants' => collect(['first' => $this->quadrant1, 'second' => $this->quadrant3]),
            'CheckCollisions' => new BridgeCommand(new MacroCommand([])),
        ]);
        $obj2 = $this->createMock(IUObject::class);
        $obj2->expects($this->never())->method('get');
        $obj3 = $this->createMock(IUObject::class);
        $obj3->expects($this->once())->method('get')->willReturn(new Vector([1, 0]));
        $obj4 = $this->createMock(IUObject::class);
        $obj4->expects($this->once())->method('get')->willReturn(new Vector([2, 0]));
        $obj5 = $this->createMock(IUObject::class);
        $obj5->expects($this->once())->method('get')->willReturn(new Vector([3, 0]));
        $this->movable1 = $this->ioc->resolve('Adapter', IQuadrantMovable::class, $obj1);
        $movable2 = $this->ioc->resolve('Adapter', IQuadrantMovable::class, $obj2);
        $movable3 = $this->ioc->resolve('Adapter', IQuadrantMovable::class, $obj3);
        $movable4 = $this->ioc->resolve('Adapter', IQuadrantMovable::class, $obj4);
        $movable5 = $this->ioc->resolve('Adapter', IQuadrantMovable::class, $obj5);
        $this->quadrant1->setObjects(collect([$this->movable1, $movable2]));
        $this->quadrant2->setObjects(collect([$movable3, $movable4, $movable5]));
        $this->quadrant3->setObjects(collect([$this->movable1, $movable3, $movable4, $movable5]));
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->movable1);
        unset($this->ioc);
        unset($this->quadrant1);
        unset($this->quadrant2);
        unset($this->quadrant3);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $this->ioc->resolve('IoCRegister', 'quadrants', function () {
            return collect([
                'first' => collect([$this->quadrant1, $this->quadrant2]),
                'second' => collect([$this->quadrant3])
            ]);
        })->execute();
        $command = new QuadrantsMove($this->movable1, $this->ioc);
        $command->execute();
        $this->assertEquals(1, $this->quadrant1->getObjects()->count());
        $this->assertEquals(4, $this->quadrant2->getObjects()->count());
        $this->movable1->getCheckCollisions()->execute();
    }
}