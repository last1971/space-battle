<?php

use Illuminate\Http\Request;
use Last1971\SpaceBattle\Middlewares\JwtToken;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use PHPUnit\Framework\TestCase;

class JwtTokenTest extends TestCase
{
    private Configuration $configuration;

    private JwtToken $jwtToken;

    protected function setUp(): void
    {
        parent::setUp();
        $this->configuration = Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText('space-battle')
        );
        $this->jwtToken = new JwtToken();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->configuration);
        unset($this->jwtToken);
    }

    public function testNotToken(): void
    {
        $request = new Request();
        $response = $this->jwtToken->handle($request, fn() => null);
        $this->assertEquals('Error Authenticate.', $response);
    }

    public function testOldToken(): void
    {
        $now = new DateTimeImmutable();
        $token = $this
            ->configuration
            ->builder()
            ->expiresAt($now->modify('-1 hour'))
            ->getToken($this->configuration->signer(), $this->configuration->signingKey());
        $request = new Request();
        $request->headers->set('Authorization', 'Bearer ' . $token->toString());
        $response = $this->jwtToken->handle($request, fn() => null);
        $this->assertEquals('Token is rancid', $response);
    }

    public function testBadGameId(): void
    {
        $now = new DateTimeImmutable();
        $token = $this
            ->configuration
            ->builder()
            ->expiresAt($now->modify('+1 hour'))
            ->getToken($this->configuration->signer(), $this->configuration->signingKey());
        $request = new Request(['gameId' => 123]);
        $request->headers->set('Authorization', 'Bearer ' . $token->toString());
        $response = $this->jwtToken->handle($request, fn() => null);
        $this->assertEquals('Bad gameId', $response);
    }

    public function testSuccess(): void
    {
        $now = new DateTimeImmutable();
        $token = $this
            ->configuration
            ->builder()
            ->expiresAt($now->modify('+1 hour'))
            ->withClaim('gameId', 123)
            ->getToken($this->configuration->signer(), $this->configuration->signingKey());
        $request = new Request(['gameId' => 123]);
        $request->headers->set('Authorization', 'Bearer ' . $token->toString());
        $response = $this->jwtToken->handle($request, fn() => 'Success');
        $this->assertEquals('Success', $response);
    }
}