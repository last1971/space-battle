<?php

namespace Tests;

use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Commands\HardStopCommand;
use Last1971\SpaceBattle\Commands\RunCommand;
use Last1971\SpaceBattle\Handlers\CommonHandler;
use Last1971\SpaceBattle\Handlers\ExceptionHandler;
use Last1971\SpaceBattle\Handlers\MoveToHandler;
use Last1971\SpaceBattle\Interfaces\ICommand;
use PHPUnit\Framework\TestCase;

class MoveToHandlerTest extends TestCase
{
    /**
     * @var Collection
     */
    private Collection $queue;

    /**
     * @var Collection
     */
    private Collection $states;

    /**
     * @var ExceptionHandler
     */
    private ExceptionHandler $exceptionHandler;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->queue = collect();
        $this->exceptionHandler = new ExceptionHandler();
        $this->states = collect([
            HardStopCommand::class => null,
            RunCommand::class => new CommonHandler($this->exceptionHandler, $this->queue, collect()),
        ]);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->queue);
        unset($this->exceptionHandler);
        unset($this->states);
    }

    /**
     * @return void
     */
    public function testRun(): void
    {
        $command = new RunCommand();
        $this->queue->push($command);
        $handler = new MoveToHandler($this->queue, $this->states, collect());
        $newHandler = $handler->handle();
        $this->assertEquals(CommonHandler::class, get_class($newHandler));
    }

    /**
     * @return void
     */
    public function testHardStop(): void
    {
        $command1 = $this->createMock(ICommand::class);
        $command1->expects($this->never())->method('execute');
        $command2 = new HardStopCommand();
        $newQueue = collect();
        $this->queue->push($command1);
        $this->queue->push($command2);
        $handler = new MoveToHandler($this->queue, $this->states, $newQueue);
        $handler1 = $handler->handle();
        $this->assertEquals($command1, $newQueue->first());
        $this->assertEquals($handler, $handler1);
        $handler2 = $handler1->handle();
        $this->assertEquals(null, $handler2);
    }
}