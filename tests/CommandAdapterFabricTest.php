<?php

namespace Tests;

use Last1971\SpaceBattle\Base\CommandAdapterFabric;
use Last1971\SpaceBattle\Base\DynamicAdapterClass;
use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Base\IoCRegister;
use Last1971\SpaceBattle\Base\Scope;
use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\IoCCommand;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IMovable;
use Last1971\SpaceBattle\Interfaces\IUObject;
use PHPUnit\Framework\TestCase;

class CommandAdapterFabricTest extends TestCase
{
    /**
     * @var CommandAdapterFabric
     */
    private CommandAdapterFabric $commandAdapterFabric;

    /**
     * @var IoC
     */
    private IoC $ioc;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $uObject = $this->createMock(IUObject::class);
        $uObject->method('get')->willReturnCallback(function ($key) {
            return $key === 'Position'? new Vector([1, 2]) : new Vector([2, 1]);
        });
        $uObject->method('set')->willReturnCallback(function (string $key, Vector $newPosition) {
            $this->assertEquals('Position', $key);
            $this->assertEquals(new Vector([1, -1]), $newPosition);
        });
        $iocRegister = new IoCRegister();
        $scope = new Scope($iocRegister);
        $this->ioc = new IoC($scope);
        $this->ioc->resolve('IoCRegister', 'IMovableAdapter->getPosition', function (IUObject $object) {
            return $object->get('Position');
        })->execute();
        $this->ioc->resolve('IoCRegister', 'IMovableAdapter->getVelocity', function (IUObject $object) {
            return $object->get('Velocity');
        })->execute();
        $this->ioc->resolve(
            'IoCRegister',
            'IMovableAdapter->setPosition',
            function (IUObject $object, Vector $newPosition) {
                return new IoCCommand(function () use ($object, $newPosition){
                    $object->set('Position', $newPosition);
                });
            },
        )->execute();
        $this->commandAdapterFabric = new CommandAdapterFabric(
            IMovable::class,
            $uObject,
            $this->ioc,
            new DynamicAdapterClass()
        );
        DynamicAdapterClass::iocRegister($this->ioc);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->commandAdapterFabric);
        unset($this->ioc);
    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    public function testGenerate(): void
    {
        /** @var IMovable $movable */
        $movable = $this->commandAdapterFabric->generate();
        $this->assertTrue($movable instanceof IMovable);
        $this->assertEquals('IMovableAdapter', get_class($movable));
        $this->assertEquals(new Vector([1, 2]), $movable->getPosition());
        $this->assertEquals(new Vector([2, 1]), $movable->getVelocity());
        $movable->setPosition(new Vector([1, -1]));
        $otherMovable = $this->commandAdapterFabric->generate();
        $this->assertEquals($movable, $otherMovable);
    }

    /**
     * @return void
     */
    public function testIoCResolveAdapter(): void
    {
        $uObject1 = $this->createMock(IUObject::class);
        $uObject1->method('get')->willReturnCallback(function ($key) {
            return $key === 'Position'? new Vector([1, 1]) : new Vector([2, 2]);
        });
        $uObject2 = $this->createMock(IUObject::class);
        $uObject2->method('get')->willReturnCallback(function ($key) {
            return $key === 'Position'? new Vector([2, 2]) : new Vector([1, 1]);
        });
        /** @var IMovable $movable1 */
        $movable1 = $this->ioc->resolve('Adapter', IMovable::class, $uObject1);
        $this->assertEquals(new Vector([1, 1]), $movable1->getPosition());
        /** @var IMovable $movable2 */
        $movable2 = $this->ioc->resolve('Adapter', IMovable::class, $uObject2);
        $this->assertEquals(new Vector([2, 2]), $movable2->getPosition());
    }

    /**
     * @return void
     */
    public function testThirdTask(): void
    {
        $uObject = $this->createMock(IUObject::class);
        $uObject->expects($this->once())->method('get')->willReturnCallback(function (string $key) {
            $this->assertEquals('Test', $key);
        });
        $this->ioc->resolve('IoCRegister', 'ICommandAdapter->execute', function (IUObject $object) {
            return new IoCCommand(function () use ($object){
                $object->get('Test');
            });
        })->execute();
        /** @var ICommand $commandable */
        $commandable = $this->ioc->resolve('Adapter', ICommand::class, $uObject);
        $commandable->execute();
    }
}