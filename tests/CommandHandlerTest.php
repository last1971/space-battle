<?php

namespace Tests;

use Last1971\SpaceBattle\Handlers\CommandHandler;
use Last1971\SpaceBattle\Handlers\ExceptionHandler;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExceptionHandler;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class CommandHandlerTest extends TestCase
{
    /**
     * @var CommandHandler
     */
    private CommandHandler $commandHandler;

    /**
     * @var ExceptionHandler
     */
    private ExceptionHandler $exceptionHandler;

    /**
     * @var Collection
     */
    private Collection $commandQueue;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->exceptionHandler = new ExceptionHandler();
        $this->commandQueue = collect();
        $this->commandHandler =new CommandHandler($this->exceptionHandler, $this->commandQueue);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->commandHandler);
        unset($this->commandQueue);
        unset($this->exceptionHandler);
    }

    /**
     * @return void
     */
    public function testHandleException(): void
    {
        $testCommand = $this->createMock(ICommand::class);
        $testException = new \BadMethodCallException();
        $testCommand->method('execute')->willThrowException($testException);
        $exceptionHandler = $this->createMock(IExceptionHandler::class);
        $exceptionHandler
            ->method('handle')
            ->willReturnCallback(
                function(ICommand $command, \Exception $exception, Collection $queue)
                use ($testCommand, $testException) {
                    $this->assertEquals($testCommand, $command);
                    $this->assertEquals($testException, $exception);
                    $this->assertEquals($this->commandQueue, $queue);
                }
            );
        $this->commandQueue->push($testCommand);
        $this
            ->exceptionHandler
            ->addAction(get_class($testCommand), \BadMethodCallException::class, $exceptionHandler);
        $this->commandHandler->handle();
    }

}