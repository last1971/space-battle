<?php

namespace Tests;

use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Base\IoCRegister;
use Last1971\SpaceBattle\Base\Scope;
use Last1971\SpaceBattle\Commands\MacroCommand;
use Last1971\SpaceBattle\Commands\Move;
use Last1971\SpaceBattle\Commands\Rotate;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IMovable;
use Last1971\SpaceBattle\Interfaces\IRotatable;
use PHPUnit\Framework\TestCase;
use stdClass;

class IoCTest extends TestCase
{
    /**
     * @var IoC
     */
    private IoC $ioc;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $iocRegister = new IoCRegister();
        $scope = new Scope($iocRegister);
        $this->ioc = new IoC($scope);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->ioc);
    }

    /**
     * @return void
     */
    public function testIoCRegister(): void
    {
        $movable = $this->createMock(IMovable::class);
        $result = $this->ioc->resolve('IoCRegister', 'Move', function (IMovable $m) {
            return new Move($m);
        });
        $this->assertInstanceOf(ICommand::class, $result);
        $result->execute();
        $this->assertInstanceOf(Move::class, $this->ioc->resolve('Move', $movable));
    }

    /**
     * @return void
     */
    public function testRegisterBaseClass(): void
    {
        $movable = $this->createMock(IMovable::class);
        $this->assertInstanceOf(
            Move::class,
            $this->ioc->resolve('Last1971\\SpaceBattle\\Commands\\Move', $movable)
        );
    }

    /**
     * @return void
     */
    public function testIoCRegisterNotChanged(): void
    {
        $this->ioc->resolve('IoCRegister', 'IoCRegister', function () {
            return new stdClass();
        })->execute();
        $result = $this->ioc->resolve('IoCRegister');
        $this->assertInstanceOf(ICommand::class, $result);
        $result->execute();
    }

    /**
     * @return void
     */
    public function testScopeNewNotChanged(): void
    {
        $this->ioc->resolve('IoCRegister', 'ScopesNew', function () {
            return new stdClass();
        })->execute();
        $result = $this->ioc->resolve('ScopesNew');
        $this->assertInstanceOf(ICommand::class, $result);

    }

    /**
     * @return void
     */
    public function testScopesNew(): void
    {
        $movable = $this->createMock(IMovable::class);
        $rotatable = $this->createMock(IRotatable::class);
        $this->ioc->resolve('IoCRegister', 'Move', function (IMovable $m) {
            return new Move($m);
        })->execute();
        $this->ioc->resolve('ScopesCurrent', 'other')->execute();
        $this->ioc->resolve('IoCRegister', 'Move', function (IMovable $m, IRotatable $r) {
            return new MacroCommand([ new Move($m), new Rotate($r)]);
        })->execute();
        //var_dump($this->ioc->getScopesCurrent(), $this->ioc->getState()); exit;
        $this->assertInstanceOf(MacroCommand::class, $this->ioc->resolve('Move', $movable, $rotatable));
        $this->ioc->resolve('ScopesCurrent', 'default')->execute();
        $this->assertInstanceOf(Move::class, $this->ioc->resolve('Move', $movable, $rotatable));
    }
}