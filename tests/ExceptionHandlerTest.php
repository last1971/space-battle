<?php

namespace Tests;

use Last1971\SpaceBattle\Handlers\ExceptionHandler;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Last1971\SpaceBattle\Interfaces\IExceptionHandler;
use Illuminate\Support\Collection;

class ExceptionHandlerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ExceptionHandler
     */
    private ExceptionHandler $exceptionHandler;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->exceptionHandler = new ExceptionHandler();
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->exceptionHandler);
    }

    /**
     * @return void
     */
    public function testObtain(): void
    {
        $testCommand = $this->createMock(ICommand::class);
        $testException = $this->createMock(\Exception::class);
        $testQueue = collect();
        $exceptionHandler = $this->createMock(IExceptionHandler::class);
        $exceptionHandler
            ->method('handle')
            ->willReturnCallback(
                function (ICommand $command, \Exception $exception, Collection $queue)
                use ($testException, $testCommand, $testQueue) {
                    $this->assertEquals($testQueue, $queue);
                    $this->assertEquals($testCommand, $command);
                    $this->assertEquals($testException, $exception);
                }
            );
        $this->exceptionHandler->addAction(get_class($testCommand), get_class($testException), $exceptionHandler);
        $this->exceptionHandler->obtain($testCommand, $testException, $testQueue);
    }

}