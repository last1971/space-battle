<?php

namespace Tests;

use Illuminate\Http\Request;
use Last1971\AuthServer\Middlewares\Auth;
use PHPUnit\Framework\TestCase;

class AuthTest extends TestCase
{
    private Auth $auth;

    protected function setUp(): void
    {
        parent::setUp();
        $this->auth = new Auth();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->auth);
    }

    /**
     * @return void
     */
    public function testBadCredentials(): void
    {
        $request = new Request([
            'name' => 'Third',
            'password' => '',
        ]);
        $response = $this->auth->handle($request, fn() => null);
        $this->assertEquals('Bad credentials', $response);
    }

    public function testHandle(): void
    {
        $request = new Request([
            'name' => 'Second',
            'password' => 'password',
        ]);
        $this->auth->handle($request, function (Request $req) {
            $this->assertNotEmpty($req->user());
        });
    }
}