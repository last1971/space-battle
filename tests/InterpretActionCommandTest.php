<?php

namespace Tests;

use Error;
use Exception;
use Last1971\SpaceBattle\Base\DynamicAdapterClass;
use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Base\IoCRegister;
use Last1971\SpaceBattle\Base\Scope;
use Last1971\SpaceBattle\Base\UObject;
use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\InterpertActionCommand;
use Last1971\SpaceBattle\Commands\IoCCommand;
use Last1971\SpaceBattle\Interfaces\IUObject;
use PHPUnit\Framework\TestCase;

class InterpretActionCommandTest extends TestCase
{

    /**
     * @var IoC
     */
    private IoC $ioc;

    /**
     * @var Scope
     */
    private Scope $scope;

    /**
     * @var IUObject
     */
    private IUObject $object;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $iocRegister = new IoCRegister();
        $this->scope = new Scope($iocRegister);
        $this->ioc = new IoC($this->scope);
        $this->ioc->resolve('IoCRegister', 'IMovableAdapter->getPosition', function (IUObject $object) {
            return $object->get('Position');
        })->execute();
        $this->ioc->resolve('IoCRegister', 'IMovableAdapter->getVelocity', function (IUObject $object) {
            return $object->get('Velocity');
        })->execute();
        $this->ioc->resolve(
            'IoCRegister',
            'IMovableAdapter->setPosition',
            function (IUObject $object, Vector $newPosition) {
                return new IoCCommand(function () use ($object, $newPosition){
                    $object->set('Position', $newPosition);
                });
            },
        )->execute();
        DynamicAdapterClass::iocRegister($this->ioc);
        $this->object = new UObject([
            'Velocity' => new Vector([1, 1]),
            'Position' => new Vector([1, 1])
        ]);
        $this->ioc->resolve('ScopesCurrent', 'Player1')->execute();
        $this->ioc->resolve('IoCRegister', 'GetObject', function () {
            return $this->object;
        })->execute();
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->object);
        unset($this->ioc);
        unset($this->scope);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $context = new UObject([
            'id' => 548,
            'action' => 'StartMove',
            'initialVelocity' => 2,
        ]);
        $command = new InterpertActionCommand($context, $this->ioc);
        $command->execute();
        $this->assertEquals(new Vector([3, 3]), $this->object->get('Position'));
    }

    /**
     * @return void
     */
    public function testException(): void
    {
        $this->expectException(Error::class);
        $context = new UObject([
            'id' => 548,
            'action' => 'StartMove',
            'initialVelocity' => 2,
        ]);
        $this->ioc->resolve('ScopesCurrent', 'default')->execute();
        $this->ioc->resolve('ScopesNew', 'Player2')->execute();
        $command = new InterpertActionCommand($context, $this->ioc);
        $command->execute();
    }
}