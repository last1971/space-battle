<?php

namespace Tests;

use Exception;
use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Commands\Flow;
use Last1971\SpaceBattle\Commands\HardStop;
use Last1971\SpaceBattle\Commands\SoftStop;
use Last1971\SpaceBattle\Commands\Tick;
use Last1971\SpaceBattle\Commands\TickBroken;
use Last1971\SpaceBattle\Commands\TickWithEnd;
use Last1971\SpaceBattle\Handlers\CommandHandler;
use Last1971\SpaceBattle\Handlers\ExceptionHandler;
use Last1971\SpaceBattle\Interfaces\ICommand;
use PHPUnit\Framework\TestCase;

class TickTest extends TestCase
{
    /**
     * @var Tick
     */
    private Tick $tick;

    /**
     * @var TickWithEnd
     */
    private TickWithEnd $tickWithEnd;

    /**
     * @var TickBroken
     */
    private TickBroken $tickBroken;

    /**
     * @var Collection
     */
    private Collection $commandQueue;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->commandQueue = collect();
        $commandHandler = new CommandHandler(new ExceptionHandler(), $this->commandQueue);
        $this->tick = new Tick($commandHandler);
        $this->tickWithEnd = new TickWithEnd($commandHandler);
        $this->tickBroken = new TickBroken($commandHandler);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->tickBroken);
        unset($this->tickWithEnd);
        unset($this->tick);
        unset($this->commandQueue);
    }

    /**
     * @return void
     */
    public function testHardStop(): void
    {
        $testCommand = $this->createMock(ICommand::class);
        $testCommand->expects($this->exactly(2))->method('execute');
        $exceptionCommand = $this->createMock(ICommand::class);
        $exceptionCommand->method('execute')->willThrowException(new Exception('Test'));
        $hardStop = new HardStop($this->tick, $this->tickBroken);
        $this->commandQueue->push($testCommand, $exceptionCommand, $testCommand, $hardStop, $testCommand);
        $flow = new Flow($this->tick);
        $flow->execute();
        $this->assertEquals(1, $this->commandQueue->count());
        $this->assertEquals(Flow::STOP, $flow->getState());
    }

    /**
     * @return void
     */
    public function testSoftStop(): void
    {
        $testCommand = $this->createMock(ICommand::class);
        $testCommand->expects($this->exactly(3))->method('execute');
        $softStop = new SoftStop($this->tick, $this->tickWithEnd);
        $flow = new Flow($this->tick);
        $testFlow = $this->createMock(ICommand::class);
        $testFlow->method('execute')->willReturnCallback(function () use ($flow) {
            $this->assertEquals(Flow::START, $flow->getState());
        });
        $this->commandQueue->push($testCommand, $softStop, $testFlow, $testCommand, $testCommand);
        $flow->execute();
        $this->assertEquals(0, $this->commandQueue->count());
        $this->assertEquals(Flow::STOP, $flow->getState());
    }
}