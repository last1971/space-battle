<?php

namespace Tests;

use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\BurnFuel;
use Last1971\SpaceBattle\Commands\CheckFuel;
use Last1971\SpaceBattle\Commands\Move;
use Last1971\SpaceBattle\Commands\MoveWithBurnFuel;
use Last1971\SpaceBattle\Exceptions\CommandException;
use Last1971\SpaceBattle\Interfaces\IFuelable;
use Last1971\SpaceBattle\Interfaces\IMovable;
use PHPUnit\Framework\MockObject\MockObject as MockObjectAlias;
use PHPUnit\Framework\TestCase;

class MoveWithBurnFuelTest extends TestCase
{
    /**
     * @var IMovable|MockObjectAlias
     */
    private IMovable $movable;

    /**
     * @var IFuelable|MockObjectAlias
     */
    private IFuelable $fuelable;

    /**
     * @var MoveWithBurnFuel
     */
    private MoveWithBurnFuel $moveWithBurnFuel;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->movable = $this->createMock(IMovable::class);
        $this->fuelable = $this->createMock(IFuelable::class);
        $this->moveWithBurnFuel = new MoveWithBurnFuel(
            new CheckFuel($this->fuelable),
            new Move($this->movable),
            new BurnFuel($this->fuelable),
        );
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->moveWithBurnFuel);
        unset($this->fuelable);
        unset($this->movable);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $this->movable->expects($this->once())->method('getPosition')->willReturn(new Vector([1, 1]));
        $this->movable->expects($this->once())->method('getVelocity')->willReturn(new Vector([2, -2]));
        $this->movable
            ->expects($this->once())
            ->method('setPosition')
            ->willReturnCallback(function (Vector $newPosition) {
                $this->assertEquals(new Vector([3, -1]), $newPosition);
            });
        $this->fuelable->expects($this->exactly(2))->method('getFuel')->willReturn(3);
        $this->fuelable->expects($this->exactly(2))->method('getFuelConsumption')->willReturn(2);
        $this->fuelable
            ->expects($this->once())
            ->method('setFuel')
            ->willReturnCallback(function (int $fuel) {
                $this->assertEquals(1, $fuel);
            });
        $this->moveWithBurnFuel->execute();
    }

    /**
     * @return void
     */
    public function testExecuteException(): void
    {
        $this->movable->expects($this->never())->method('getPosition');
        $this->movable->expects($this->never())->method('getVelocity');
        $this->movable->expects($this->never())->method('setPosition');
        $this->fuelable->expects($this->exactly(1))->method('getFuel')->willReturn(2);
        $this->fuelable->expects($this->exactly(1))->method('getFuelConsumption')->willReturn(3);
        $this->expectException(CommandException::class);
        $this->moveWithBurnFuel->execute();
    }
}