<?php

namespace Tests;

use Last1971\SpaceBattle\Base\Direction;
use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\ChangeVelocity;
use Last1971\SpaceBattle\Interfaces\IVelocitable;
use PHPUnit\Framework\TestCase;

class ChangeVelocityTest extends TestCase
{
    /**
     * @var IVelocitable|\PHPUnit\Framework\MockObject\MockObject
     */
    private IVelocitable $velocitable;

    /**
     * @var ChangeVelocity
     */
    private ChangeVelocity $changeVelocity;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->velocitable = $this->createMock(IVelocitable::class);
        $this->changeVelocity = new ChangeVelocity($this->velocitable);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->changeVelocity);
        unset($this->velocitable);
    }

    /**
     * @throws \Exception
     */
    public function testExecute(): void
    {
        $this->velocitable->method('getVelocity')->willReturn(new Vector([0, 2]));
        $this->velocitable->method('getDirection')->willReturn(new Direction(1, 8));
        $this->velocitable->method('setVelocity')->willReturnCallback(function (Vector $newVelocity) {
            $this->assertEquals(new Vector([2, 2]), $newVelocity);
        });
        $this->changeVelocity->execute();
    }
}