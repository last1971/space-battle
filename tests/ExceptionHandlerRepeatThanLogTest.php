<?php

namespace Tests;

use Last1971\SpaceBattle\Commands\LogCommand;
use Last1971\SpaceBattle\Commands\Move;
use Last1971\SpaceBattle\Commands\RepeatCommand;
use Last1971\SpaceBattle\Handlers\ExceptionHandlerRepeatThanLog;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Monolog\Logger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ExceptionHandlerRepeatThanLogTest extends TestCase
{
    /**
     * @var ExceptionHandlerRepeatThanLog
     */
    private  ExceptionHandlerRepeatThanLog $handler;

    /**
     * @var ICommand|Move|MockObject
     */
    private ICommand $command;

    /**
     * @var \Exception|MockObject
     */
    private \Exception $exception;

    private Logger $logger;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->exception = $this->createMock(\Exception::class);
        $this->command = $this->createMock(ICommand::class);
        $this->logger = $this->createMock(Logger::class);
        $this->handler = new ExceptionHandlerRepeatThanLog($this->logger);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->handler);
        unset($this->logger);
        unset($this->command);
        unset($this->exception);
    }

    /**
     * @return void
     */
    public function testHandle(): void
    {
        $queue = collect();
        $this->handler->handle($this->command, $this->exception, $queue);
        $this->assertInstanceOf(RepeatCommand::class, $queue->last());
        $this->handler->handle($queue->shift(), $this->exception, $queue);
        $this->assertInstanceOf(LogCommand::class, $queue->last());
    }
}