<?php

namespace Tests;

use Illuminate\Http\Request;
use Last1971\SpaceBattle\DTOs\CommandMessageDTO;
use PHPUnit\Framework\TestCase;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;
use TypeError;

class CommandMessageDTOTest extends TestCase
{
    /**
     * @throws UnknownProperties
     */
    public function testFromRequestException(): void
    {
        $request = Request::create('/operation', 'GET', ['gameId' => '123']);
        $this->expectException(TypeError::class);
        CommandMessageDTO::fromRequest($request);
    }

    /**
     * @throws UnknownProperties
     */
    public function testFromRequest(): void
    {
        $request = Request::create(
            '/operation',
            'GET',
            ['gameId' => '123', 'objectId' => 'SH1', 'operationId' => 'move'],
        );
        $dto = CommandMessageDTO::fromRequest($request);
        $this->assertInstanceOf(CommandMessageDTO::class, $dto);
    }
}