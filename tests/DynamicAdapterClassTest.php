<?php

namespace Tests;

use Last1971\SpaceBattle\Base\DynamicAdapterClass;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class DynamicAdapterClassTest extends TestCase
{
    /**
     * @var DynamicAdapterClass
     */
    private DynamicAdapterClass $class;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->class = new DynamicAdapterClass();
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->class);
    }

    /**
     * @return void
     */
    public function testDetClassName(): void
    {
        $reflectionClass = $this->createMock(ReflectionClass::class);
        $reflectionClass->method('getShortName')->willReturn('Test');
        $this->assertEquals('TestAdapter', $this->class->getClassName($reflectionClass));
    }

    /**
     * @return void
     */
    public function testCreateConstructor(): void
    {

        $this->assertEquals(
            'private Last1971\SpaceBattle\Interfaces\IUObject $uObject; private Last1971\SpaceBattle\Base\IoC $ioc; public function __construct(Last1971\SpaceBattle\Interfaces\IUObject $uObject, Last1971\SpaceBattle\Base\IoC $ioc) { $this->uObject = $uObject; $this->ioc = $ioc; } ',
            $this->class->createConstructor()
        );
    }
}