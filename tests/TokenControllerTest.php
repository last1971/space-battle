<?php

namespace Tests;

use Illuminate\Http\Request;
use Last1971\AuthServer\Controller\TokenController;
use Last1971\AuthServer\Middlewares\Auth;
use Last1971\SpaceBattle\Base\CommandAdapterFabric;
use Last1971\SpaceBattle\Base\UObject;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use PHPUnit\Framework\TestCase;

class TokenControllerTest extends TestCase
{
    /**
     * @var TokenController
     */
    private TokenController $controller;

    /**
     * @var Auth
     */
    private Auth $auth;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $battles = [
            '1' => new UObject([
                'id' => '1',
                'userIds' => ['2', '3']
            ]),
            '2' => new UObject([
                'id' => '2',
                'userIds' => ['3', '4']
            ]),
        ];
        $this->controller = new TokenController($battles);
        $this->auth = new Auth();
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->controller);
        unset($this->auth);
    }

    /**
     * @return void
     */
    public function testNotBattleId(): void
    {
        $request = new Request([
            'name' => 'Second',
            'password' => 'password',
        ]);
        $response = $this->auth->handle($request, $this->controller->create(...));
        $this->assertEquals('Bad gameId', $response);
    }

    /**
     * @return void
     */
    public function testBadUserId(): void
    {
        $request = new Request([
            'name' => 'First',
            'password' => 'password',
            'gameId' => '1',
        ]);
        $response = $this->auth->handle($request, $this->controller->create(...));
        $this->assertEquals('Bad user', $response);
    }

    /**
     * @return void
     */
    public function testCreate(): void
    {
        $request = new Request([
            'name' => 'Second',
            'password' => 'password',
            'gameId' => '1',
        ]);
        $response = $this->auth->handle($request, $this->controller->create(...));
        $configuration = Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText('space-battle')
        );
        $parsed = $configuration->parser()->parse($response);
        $this->assertEquals('1', $parsed->claims()->get('gameId'));
    }
}