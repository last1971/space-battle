<?php

namespace Tests;

use Last1971\SpaceBattle\Commands\BridgeCommand;
use Last1971\SpaceBattle\Interfaces\ICommand;

class BridgeCommandTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @return void
     */
    public function testExecute(): void
    {
        $command1 = $this->createMock(ICommand::class);
        $command1->expects($this->once())->method('execute');
        $command2 = $this->createMock(ICommand::class);
        $command2->expects($this->once())->method('execute');
        $bridge = new BridgeCommand($command1);
        $bridge->execute();
        $bridge->inject($command2);
        $bridge->execute();
    }
}