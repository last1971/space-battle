<?php

namespace Tests;

use Last1971\SpaceBattle\Base\Vector;
use PHPUnit\Framework\TestCase;

class VectorTest extends TestCase
{
    /**
     * @var Vector
     */
    private Vector $vector;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->vector = new Vector();
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->vector);
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testPlusSuccess(): void
    {
        $this->vector->setCoordinates([1, 2, -1]);
        $this->assertEquals(new Vector([2, 3, 0]), Vector::plus($this->vector,  new Vector([1, 1, 1])));
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testPlusError(): void
    {
        $this->expectExceptionMessage('Different coordinate systems');
        $this->vector->setCoordinates([1, 2, -1]);
        Vector::plus($this->vector,  new Vector(['x' => 1, 'y' => 1, 'z' => 1]));
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testXYCoordinates():void
    {
        $v1 = new Vector(['x' => 1, 'y' => -1]);
        $v2 = new Vector(['y' => 1, 'x' => -1]);
        $this->assertEquals(new Vector(['x' => 0, 'y' => 0]), Vector::plus($v1, $v2));
    }

}