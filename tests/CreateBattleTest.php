<?php

namespace Tests;

use Exception;
use Last1971\AuthServer\Commands\CreateBattle;
use Last1971\AuthServer\Interfaces\IBattleable;
use PHPUnit\Framework\TestCase;

class CreateBattleTest extends TestCase
{
    public function testException(): void
    {
        $battleable = $this->createMock(IBattleable::class);
        $this->expectException(Exception::class);
        $command = new CreateBattle($battleable);
        $command->execute();
    }

    public function testExecute(): void
    {
        $battleable = $this->createMock(IBattleable::class);
        $battleable->method('getUserIds')->willReturn(['1', '2', '3']);
        $battleable
            ->expects($this->once())
            ->method('setId')
            ->willReturnCallback(function ($id) {
                $this->assertIsString($id);
            });
        $command = new CreateBattle($battleable);
        $command->execute();
    }
}