<?php

namespace Tests;

use Last1971\SpaceBattle\Commands\Move;
use Last1971\SpaceBattle\Commands\RepeatCommand;
use Last1971\SpaceBattle\Handlers\ExceptionHandlerRepeat;
use Last1971\SpaceBattle\Interfaces\ICommand;
use PHPUnit\Framework\MockObject\MockObject;

class ExceptionHandlerRepeatTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ExceptionHandlerRepeat
     */
    private  ExceptionHandlerRepeat $handler;

    /**
     * @var ICommand|Move|MockObject
     */
    private ICommand $command;

    /**
     * @var \Exception|MockObject
     */
    private \Exception $exception;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->exception = $this->createMock(\Exception::class);
        $this->command = $this->createMock(ICommand::class);
        $this->handler = new ExceptionHandlerRepeat();
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->handler);
        unset($this->command);
        unset($this->exception);
    }

    /**
     * @return void
     */
    public function testHandle(): void
    {
        $queue = collect([1, 2, 3]);
        $this->handler->handle($this->command, $this->exception, $queue);
        $this->assertEquals(new RepeatCommand($this->command), $queue->last());
    }

}