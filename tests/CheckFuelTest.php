<?php

namespace Tests;

use Last1971\SpaceBattle\Commands\CheckFuel;
use Last1971\SpaceBattle\Exceptions\CommandException;
use Last1971\SpaceBattle\Interfaces\IFuelable;
use PHPUnit\Framework\MockObject\MockObject as MockObjectAlias;

class CheckFuelTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var CheckFuel
     */
    private CheckFuel $checkFuel;

    /**
     * @var IFuelable|MockObjectAlias
     */
    private IFuelable $fuelable;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->fuelable = $this->createMock(IFuelable::class);
        $this->checkFuel = new CheckFuel($this->fuelable);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->checkFuel);
        unset($this->fuelable);
    }

    /**
     * @return void
     * @throws CommandException
     */
    public function testExecuteException(): void
    {
        $this->fuelable->method('getFuel')->willReturn(1);
        $this->fuelable->method('getFuelConsumption')->willReturn(2);
        $this->expectException(CommandException::class);
        $this->checkFuel->execute();
    }

    /**
     * @return void
     * @throws CommandException
     */
    public function testExecute(): void
    {
        $this->fuelable->method('getFuel')->willReturn(2);
        $this->fuelable->method('getFuelConsumption')->willReturn(2);
        $this->expectNotToPerformAssertions();
        $this->checkFuel->execute();
    }
}