<?php

namespace Tests;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Last1971\SpaceBattle\Base\DynamicAdapterClass;
use Last1971\SpaceBattle\Base\IoC;
use Last1971\SpaceBattle\Base\IoCRegister;
use Last1971\SpaceBattle\Base\Scope;
use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\InterpretCommand;
use Last1971\SpaceBattle\Commands\IoCCommand;
use Last1971\SpaceBattle\Commands\Move;
use Last1971\SpaceBattle\Commands\Rotate;
use Last1971\SpaceBattle\Controlllers\CommandMessageController;
use Last1971\SpaceBattle\DTOs\CommandMessageDTO;
use Last1971\SpaceBattle\Handlers\CommandHandler;
use Last1971\SpaceBattle\Handlers\ExceptionHandler;
use Last1971\SpaceBattle\Handlers\ExceptionHandlerException;
use Last1971\SpaceBattle\Interfaces\IMovable;
use Last1971\SpaceBattle\Interfaces\IUObject;
use Last1971\SpaceBattle\Interfaces\IVelocitable;
use PHPUnit\Framework\TestCase;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;


class CommandMessageControllerTest extends TestCase
{
    private IoC $ioc;

    private CommandMessageController $controller;

    private Request $request;

    private Collection $commandsQueue;

    private CommandHandler $commandHandler;

    protected function setUp(): void
    {
        parent::setUp();
        $scope = new Scope(new IoCRegister());
        $this->ioc = new IoC($scope);
        DynamicAdapterClass::iocRegister($this->ioc);
        $this->controller = new CommandMessageController($this->ioc);
        $this->request = Request::create(
            '/operation',
            'GET',
            ['gameId' => '123', 'objectId' => '548', 'operationId' => 'startMove', 'args' => ['Velocity' => 2]],
        );
        $commandsQueue = collect();
        $this->commandsQueue = $commandsQueue;
        $this->ioc->resolve('IoCRegister', 'IMovableAdapter->getPosition', function (IUObject $object) {
            return $object->get('Position');
        })->execute();
        $this->ioc->resolve('IoCRegister', 'IMovableAdapter->getVelocity', function (IUObject $object) {
            return $object->get('Velocity');
        })->execute();
        $this->ioc->resolve(
            'IoCRegister',
            'IMovableAdapter->setPosition', function (IUObject $object, Vector $newPosition) {
            return new IoCCommand(function () use ($object, $newPosition) {
                $object->set('Position', $newPosition);
            });
            })->execute();
        $this->ioc->resolve(
            'IoCRegister',
            'IVelocitableAdapter->setVelocity',
            function (IUObject $object, Vector $newVelocity) {
                return new IoCCommand(function () use ($object, $newVelocity) {
                    $object->set('Velocity', $newVelocity);
                });
            })->execute();
        $this->ioc->resolve('IoCRegister', 'Move', function (IUObject $object) {
            /** @var IMovable $movable */
            $movable = $this->ioc->resolve('Adapter', IMovable::class, $object);
            return new Move($movable);
        })->execute();
        $this->ioc->resolve('IoCRegister', 'SetVelocity', function (IUObject $object, int $velocity) {
            /** @var IVelocitable $velocitable */
            $velocitable = $this->ioc->resolve('Adapter', IVelocitable::class, $object);
            $velocitable->setVelocity(new Vector([$velocity, $velocity]));
            return $object;
        })->execute();
        $this->ioc->resolve('ScopesCurrent', '123')->execute();
        $this->ioc->resolve('IoCRegister', 'GetCommandsQueue', function () use ($commandsQueue) {
            return $commandsQueue;
        })->execute();
        $this->ioc->resolve('ScopesCurrent', 'default')->execute();
        $exceptionHandler = new ExceptionHandler();
        $exceptionHandler->addAction(
            InterpretCommand::class,
            Exception::class,
            new ExceptionHandlerException()
        );
        $this->commandHandler = new CommandHandler($exceptionHandler, $commandsQueue);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->controller);
        unset($this->ioc);
        unset($this->request);
        unset($this->commandHandler);
        unset($this->commandsQueue);
    }

    /**
     * @throws UnknownProperties
     */
    public function testNotGame(): void
    {
        $this->controller->commandMessage($this->request);
        $this->assertEmpty($this->commandsQueue);
    }

    /**
     * @throws UnknownProperties
     */
    public function testNotObject(): void
    {
        $this->ioc->resolve('ScopesCurrent', '123')->execute();
        $this->controller->commandMessage($this->request);
        $this->assertEmpty($this->commandsQueue);
    }

    /**
     * @throws UnknownProperties
     */
    public function testNotCommand(): void
    {
         $this->ioc->resolve('ScopesCurrent', '123')->execute();
         $objects = collect([
            '548' => $this->createMock(IUObject::class),
         ]);
         $this->ioc->resolve('IoCRegister', 'GetObjects', function (...$args) use ($objects) {
           return $objects->get($args[0]);
        })->execute();
        $this->controller->callAction('commandMessage', ['request' => $this->request]);
        $this->assertCount(1, $this->commandsQueue);
        $this->commandHandler->handle();
        $this->assertEmpty($this->commandsQueue);
    }

    /**
     * @throws UnknownProperties
     */
    public function testCommandNotAllowed(): void
    {
        $this->ioc->resolve('ScopesCurrent', '123')->execute();
        $objects = collect([
            '548' => $this->createMock(IUObject::class),
        ]);
        $this->ioc->resolve('IoCRegister', 'GetObjects', function (...$args) use ($objects) {
            return $objects->get($args[0]);
        })->execute();
        $allowedCommands = collect([
            '548' => collect(['rotate' => fn() => $this->createMock(Rotate::class)]),
        ]);
        $this->ioc->resolve(
            'IoCRegister',
            'AllowedCommands',
            function (CommandMessageDTO $dto) use ($allowedCommands) {
                return $allowedCommands->has($dto->objectId)
                    && $allowedCommands->get($dto->objectId)->has($dto->operationId);
            }
        )->execute();
        $this->controller->callAction('commandMessage', ['request' => $this->request]);
        $this->assertCount(1, $this->commandsQueue);
        $this->commandHandler->handle();
        $this->assertEmpty($this->commandsQueue);
    }

    /**
     * @throws UnknownProperties
     */
    public function testCommand(): void
    {
        $this->ioc->resolve('ScopesCurrent', '123')->execute();
        $object548 = $this->createMock(IUObject::class);
        $object548
            ->expects($this->exactly(2))
            ->method('set')
             ->willReturnCallback(function(string $key, Vector $value) {
                $this->assertEquals(new Vector([2, 2]), $value);
            });
        $map = [
            ['Velocity', new Vector([1, 1])],
            ['Position', new Vector([1, 1])]
        ];
        $object548
            ->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap($map));
        $objects = collect([
            '548' => $object548,
        ]);
        $this->ioc->resolve('IoCRegister', 'GetObjects', function (...$args) use ($objects) {
            return $objects->get($args[0]);
        })->execute();
        $allowedCommands = collect([
            '548' => collect(
                [
                    'rotate' => fn() => $this->createMock(Rotate::class),
                    'startMove' => function(CommandMessageDTO $dto) {
                        $object = $this->ioc->resolve('GetObjects', $dto->objectId);
                        if (!$dto->args || !$dto->args->get('Velocity')) {
                            throw new Exception('Velocity is empty');
                        }
                        $this->ioc->resolve('SetVelocity', $object, $dto->args->get('Velocity'));
                        $this->ioc->resolve('Move', $object)->execute();
                    }
                ]
            ),
        ]);
        $this->ioc->resolve(
            'IoCRegister',
            'AllowedCommands',
            function (CommandMessageDTO $dto) use ($allowedCommands) {
                $command = $allowedCommands->has($dto->objectId)
                    ? $allowedCommands->get($dto->objectId)->get($dto->operationId)
                    : null;
                return is_callable($command) ? $command($dto) : $command;
            }
        )->execute();
        $this->controller->callAction('commandMessage', ['request' => $this->request]);
        $this->assertCount(1, $this->commandsQueue);
        $this->commandHandler->handle();
        $this->assertEmpty($this->commandsQueue);
    }
}