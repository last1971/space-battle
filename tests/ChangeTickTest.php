<?php

namespace Tests;

use Last1971\SpaceBattle\Commands\ChangeTick;
use Last1971\SpaceBattle\Interfaces\ITickable;

class ChangeTickTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ChangeTick
     */
    private ChangeTick $changeTick;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $tickableOld = $this->createMock(ITickable::class);
        $tickableNew = $this->createMock(ITickable::class);
        $tickableOld
            ->method('changeNext')
            ->willReturnCallback(function (ITickable $newNext) use ($tickableNew) {
                $this->assertEquals($tickableNew, $newNext);
            });
        $this->changeTick = new ChangeTick($tickableOld, $tickableNew);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->changeTick);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $this->changeTick->execute();
    }
}