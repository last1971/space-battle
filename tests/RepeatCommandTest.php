<?php

namespace Tests;

use Last1971\SpaceBattle\Commands\RepeatCommand;
use Last1971\SpaceBattle\Interfaces\ICommand;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RepeatCommandTest extends TestCase
{
    /**
     * @var RepeatCommand
     */
    private RepeatCommand $repeatCommand;

    /**
     * @var ICommand|MockObject
     */
    private ICommand $command;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->command = $this->createMock(ICommand::class);
        $this->repeatCommand = new RepeatCommand($this->command);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->command);
        unset($this->repeatCommand);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $this->command->expects($this->exactly(1))->method('execute');
        $this->repeatCommand->execute();
    }
}