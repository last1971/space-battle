<?php

namespace Tests;

use Last1971\SpaceBattle\Commands\BurnFuel;
use Last1971\SpaceBattle\Interfaces\IFuelable;
use PHPUnit\Framework\MockObject\MockObject;

class BurnFuelTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var IFuelable|MockObject
     */
    private IFuelable $fuelable;

    /**
     * @var BurnFuel
     */
    private BurnFuel $burnFuel;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->fuelable = $this->createMock(IFuelable::class);
        $this->burnFuel = new BurnFuel($this->fuelable);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->burnFuel);
        unset($this->fuelable);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $this->fuelable->method('getFuel')->willReturn(10);
        $this->fuelable->method('getFuelConsumption')->willReturn(2);
        $this->fuelable->method('setFuel')->willReturnCallback(function (int $fuel) {
            $this->assertEquals(8, $fuel);
        });
        $this->burnFuel->execute();
    }
}