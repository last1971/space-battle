<?php

namespace Tests;

use Last1971\SpaceBattle\Base\Vector;
use Last1971\SpaceBattle\Commands\Move;
use Last1971\SpaceBattle\Interfaces\IMovable;
use PHPUnit\Framework\MockObject\IncompatibleReturnValueException;
use PHPUnit\Framework\TestCase;

class MoveTest extends TestCase
{
    private $movable;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->movable = $this->createMock(IMovable::class);
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->movable);
    }
    /**
     * @throws \Exception
     */

    public function testExecute(): void
    {
        $this->movable->method('getPosition')->willReturn(new Vector([-7, 3]));
        $this->movable->method('getVelocity')->willReturn(new Vector([12, 5]));
        $this->movable->method('setPosition')->willReturnCallback(function (Vector $newVector) {
            $this->assertEquals(new Vector([5, 8]), $newVector);
        });
        $move = new Move($this->movable);
        $move->execute();
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testBadPosition(): void
    {
        $this->expectException(IncompatibleReturnValueException::class);
        $this->movable->method('getPosition')->willReturn([-7, 3]);
        $move = new Move($this->movable);
        $move->execute();
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testPositionException(): void
    {
        $this->expectException(\Exception::class);
        $this->movable->method('getPosition')->will($this->throwException(new \Exception()));
        $move = new Move($this->movable);
        $move->execute();
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testBadVelocity(): void
    {
        $this->expectException(IncompatibleReturnValueException::class);
        $this->movable->method('getVelocity')->willReturn([12, 5]);
        $move = new Move($this->movable);
        $move->execute();
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testVelocityException(): void
    {
        $this->expectException(\Exception::class);
        $this->movable->method('getVelocity')->will($this->throwException(new \Exception()));
        $move = new Move($this->movable);
        $move->execute();
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testNotExecute(): void
    {
        $this->expectExceptionMessage('Different coordinate systems');
        $this->movable->method('getPosition')->willReturn(new Vector([-7, 3]));
        $this->movable->method('getVelocity')->willReturn(new Vector([12, 5, 1]));
        $move = new Move($this->movable);
        $move->execute();
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testExecuteException(): void
    {
        $this->expectException(\Exception::class);
        $this->movable->method('setPosition')->will($this->throwException(new \Exception()));
        $move = new Move($this->movable);
        $move->execute();
    }

}