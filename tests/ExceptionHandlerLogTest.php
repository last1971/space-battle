<?php

namespace Tests;

use Last1971\SpaceBattle\Commands\LogCommand;
use Last1971\SpaceBattle\Commands\Move;
use Last1971\SpaceBattle\Handlers\ExceptionHandlerLog;
use Last1971\SpaceBattle\Interfaces\ICommand;
use Monolog\Logger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ExceptionHandlerLogTest extends TestCase
{
    /**
     * @var Logger|MockObject
     */
    private Logger $logger;

    /**
     * @var ExceptionHandlerLog
     */
    private  ExceptionHandlerLog $handler;

    /**
     * @var ICommand|Move|MockObject
     */
    private ICommand $command;

    /**
     * @var \Exception|MockObject
     */
    private \Exception $exception;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->logger = $this->createMock(Logger::class);
        $this->handler = new ExceptionHandlerLog($this->logger);
        $this->command = $this->createMock(ICommand::class);
        $this->exception = $this->createMock(\Exception::class);
    }

    /**
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->logger);
        unset($this->handler);
        unset($this->command);
    }

    /**
     * @return void
     */
    public function testHandle(): void
    {
        $queue = collect([1, 2, 3]);
        $this->handler->handle($this->command, $this->exception, $queue);
        $this->assertEquals(new LogCommand($this->command, $this->exception, $this->logger), $queue->last());
    }

}
