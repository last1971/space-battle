<?php

namespace Tests;

use Illuminate\Http\Request;
use Last1971\AuthServer\Controller\BattleController;
use Last1971\AuthServer\Middlewares\Auth;
use PHPUnit\Framework\TestCase;

class BattleControllerTest extends TestCase
{
    public function testCreate(): void
    {
        $request = new Request([
            'name' => 'Second',
            'password' => 'password',
            'userIds' => ['1', '2', '3']
        ]);
        $auth = new Auth();
        $controller = new BattleController();
        $response = $auth->handle($request, $controller->create(...));
        $this->assertIsString($response);
    }
}